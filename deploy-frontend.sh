#!/bin/bash

# ---------------------------------------------------------------------------- #
#                      RateMyDerp! Frontend Build & Deploy                     #
# ---------------------------------------------------------------------------- #

# Builds Sass and JS source in a Node docker container and "deploys" it into
#   the backend app folder target directory

# SETUP
set -euf -o pipefail

# DIRS: SRC->TARGET
BUILD_DIR="$PWD/frontend"
SRC_DIR="$BUILD_DIR/dist"
TARGET_DIR="$PWD/backend/ratemyderp/static"

# BUILD FRONTEND
cd $BUILD_DIR
# build in container -> avoids package issues
echo "BUILD source output to $SRC_DIR"
docker run --user $(id -u):$(id -g) \
  -v $PWD/build:/../backend/ratemyderp/static \
  -v $PWD:/app -v $PWD/.npm:/.npm -v $PWD/.config:/.config -w /app \
   node:lts-alpine sh -c "npm install && npm run build"
cd -

# DEPLOY FRONTEND
# clean dir only if TARGET_DIR var is of len > 0
if [ ! -z "$TARGET_DIR" ];
then
  if [ -d "$TARGET_DIR" ];
  then 
    echo "CLEAN deploy target directory $TARGET_DIR"
    rm -rf "$TARGET_DIR"
  else
    echo "SKIP clean deploy target dir $TARGET_DIR not found"
  fi
else
  # error out
  echo "ERROR must define TARGET_DIR for output"
  exit 1
fi

# copy build source -> target
echo "DEPLOY frontend source to target"
cp -rT "$SRC_DIR" "$TARGET_DIR"

# done
echo "COMPLETE deploy frontend"