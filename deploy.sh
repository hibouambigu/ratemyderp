#!/bin/bash

# ---------------------------------------------------------------------------- #
#                        Rate My Derp deployment script                        #
# ---------------------------------------------------------------------------- #
# Handles build and deployment of source from repo to production VM.
# Does not deal with initial server setup.

# ----------------------------------- Setup ---------------------------------- #
set -euf -o pipefail

# -------------------------------- Directories ------------------------------- #
# app
APP_NAME=ratemyderp
APP_TARGET_DIR=/opt/$APP_NAME/app
APP_DATA_DIR=/var/opt/$APP_NAME
APP_LOG_DIR=/var/log/$APP_NAME
# env
VENV_DIR=/opt/$APP_NAME/venv
# backups
BACKUP_ROOT=/opt/$APP_NAME/backup

# ------------------------------ Source Env Vars ----------------------------- #
source "/opt/$APP_NAME/conf/.env"

# ------------------------------ Backup Database ----------------------------- #
echo "BACKING UP previous deployment..."
BACKUP_DATE=$(date '+%Y-%m-%d%H:%M:%S')
# create directories if they do not exist
if [ ! -d "$BACKUP_ROOT/previous-deploy" ];
then 
  mkdir -p "$BACKUP_ROOT/previous-deploy" "$BACKUP_ROOT/previous-deploy/media"
fi
# ensure pgpass file is configured
if [ ! -f ~/.pgpass ];
then
  echo "ERROR! .pgpass file not found. Can't back up database without one."
  exit 1
fi
# backup pre-deployment database
pg_dump ratemyderpdb -U ratemyderp -h localhost -Ft -w \
  > "$BACKUP_ROOT/previous-deploy/db/ratemyderpdb_$BACKUP_DATE.tar"
# backup media files
cp -r "$APP_DATA_DIR/media" "$BACKUP_ROOT/previous-deploy/"

# -------------------------- Set Backup Permissions -------------------------- #
# users in backups group can access backup files
chmod 775 -R "$BACKUP_ROOT/previous-deploy"
chgrp -R backups "$BACKUP_ROOT/previous-deploy"

# -------------------------- Frontend Build & Deploy ------------------------- #
echo "FRONTEND build and deploy..."
source $PWD/deploy-frontend.sh
echo "FRONTEND deploy complete!"

# ------------------------------- Clean Target ------------------------------- #
# clean dir only if APP_TARGET_DIR var is defined
if [ ! -z "$APP_TARGET_DIR" ];
then
  if [ -d "$APP_TARGET_DIR" ];
  then 
    echo "CLEANING target directory $APP_TARGET_DIR"
    rm -rf "$APP_TARGET_DIR"
  else
    echo "SKIP cleaning of target dir -- $APP_TARGET_DIR not found"
  fi
else
  # error out
  echo "ERROR! must define APP_TARGET_DIR for output"
  exit 1
fi

# -------------------------------- Copy Source ------------------------------- #
echo "DEPLOY BACKEND source to $APP_TARGET_DIR..."
cp -rT $PWD/backend $APP_TARGET_DIR

# ---------------------------- Rebuild Virtualenv ---------------------------- #
# remove existing env
if [ -d "$VENV_DIR" ];
then 
  rm -rf "$VENV_DIR"
fi
# new virtualenv
virtualenv "$VENV_DIR" --python=/usr/bin/python3.10
echo "VENV created: $VENV_DIR"

# ------------------------ Activate Venv & pip Update ------------------------ #
source "$VENV_DIR/bin/activate"
echo "INSTALL dependencies..."
# update pip -> necessary for py 3.10 on ubuntu at the moment.
echo "UPDATE pip..."
curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10 

# --------------------------- Install Requirements --------------------------- #
pip install -r "$APP_TARGET_DIR/requirements/production.txt"
echo "pip dependencies installed!"

# ---------------------------- Static Files Deploy --------------------------- #
echo "DEPLOY static files..."
python "$APP_TARGET_DIR/manage.py" collectstatic --clear --no-input



# ----------------------------- Apply Migrations ----------------------------- #
echo "MIGRATIONS apply"
python "$APP_TARGET_DIR/manage.py" migrate

# ----------------------------- Restart Services ----------------------------- #
echo "RESTARTING system services..."
systemctl restart ratemyderp celery celerybeat
systemctl list-units ratemyderp* celery* --type=service
printf "\n\n>>> DEPLOYMENT COMPLETE. <<<\n"