const { src, dest, series, parallel, watch } = require('gulp');

const rollup = require('rollup'),
  rollupNode = require('@rollup/plugin-node-resolve'),
  rollupScss = require('rollup-plugin-scss');

const del = require('del'),
  autoprefix = require('gulp-autoprefixer'),
  babel = require('gulp-babel'),
  cleanCSS = require('gulp-clean-css'),
  minify = require('gulp-minifier'),
  compileSass = require('gulp-sass')(require('sass')),
  rename = require('gulp-rename');

const BASE_DIR = {
  SRC: './src',                             // main src dir
  OUT: './dist',                            // frontend dist build output dir
  OUT_DEV: '../backend/ratemyderp/static',  // target output dir for dev mode
  BUNDLE: './bundled'                       // intermediary rollup working dir
};

function cleanBuild(cb) {
  // wipe build dirs
  return del([BASE_DIR.OUT, BASE_DIR.BUNDLE]);
}

function cleanDev(cb) {
  // clean dev output dirs
  return del([BASE_DIR.OUT_DEV, BASE_DIR.BUNDLE], {force: true});
}

function bundleBuild(cb) {
  // bundle js and sass with rollup
  return rollup
  .rollup({
    input: `${BASE_DIR.SRC}/js/main.js`,
    plugins: [
      rollupNode.nodeResolve(), 
      rollupScss(
      )
    ]
  })
  .then(bundle => {
    return bundle.write({
      file: `${BASE_DIR.BUNDLE}/main.bundled.js`,
      format: 'iife',
    });
  });
}

function bundleDev() {
  return rollup
  .rollup({
    input: `${BASE_DIR.SRC}/js/main.js`,
    plugins: [
      rollupNode.nodeResolve(), 
      rollupScss({
        sourceMap: true
      })
    ]
  })
  .then(bundle => {
    return bundle.write({
      file: `${BASE_DIR.BUNDLE}/main.bundled.js`,
      format: 'iife',
      sourcemap: true,
    });
  });
}

function cssDev(cb) {
  // prefix only for dev
  src(`${BASE_DIR.BUNDLE}/main.bundled.css`)
    .pipe(
      autoprefix({
        cascade: false,
      })
    )
    .pipe(dest(`${BASE_DIR.OUT_DEV}/bundled`));
  // copy over css sourcemap
  src(`${BASE_DIR.BUNDLE}/main.bundled.css.map`)
  .pipe(dest(`${BASE_DIR.OUT_DEV}/bundled`));
  cb();
}

function cssBuild(cb) {
  // admin site sass -> css
  src(`${BASE_DIR.SRC}/sass/admin/main.scss`)
    .pipe(
      compileSass(
        {outputStyle: 'compressed', sourceMap: false}
      ).on('error', compileSass.logError))
    .pipe(rename('admin.css'))
    .pipe(dest(`${BASE_DIR.OUT}/admin/css`));
  // (sass compilation handled by bundler)
  // -> prefix *and* minify css
  src(`${BASE_DIR.BUNDLE}/main.bundled.css`)
    .pipe(
      autoprefix({
        cascade: false,
      })
    )
    .pipe(cleanCSS())
    .pipe(dest(`${BASE_DIR.OUT}/bundled`));
  cb();
}

function jsDev(cb) {
  // copies over bundled JS and sourcemap
  src(`${BASE_DIR.BUNDLE}/main.bundled.js`)
    .pipe(dest(`${BASE_DIR.OUT_DEV}/bundled`));
  src(`${BASE_DIR.BUNDLE}/main.bundled.js.map`)
    .pipe(dest(`${BASE_DIR.OUT_DEV}/bundled`));
  cb();
}

function jsBuild(cb) {
  // transpiles and minifies bundled js
  src(`${BASE_DIR.BUNDLE}/main.bundled.js`)
    .pipe(
      babel({
        presets: ['@babel/env'],
      })
    )
    .pipe(minify({
      minify: true,
      minifyJS: {
        sourceMap: false
      }
    }))
    .pipe(dest(`${BASE_DIR.OUT}/bundled`));
  cb();
}

function imgDev(cb) {
  // image & svg assets
  src([
    `${BASE_DIR.SRC}/img/*.png`,
    `${BASE_DIR.SRC}/img/*.jpg`,
    `${BASE_DIR.SRC}/img/*.jpeg`,
  ]).pipe(dest(`${BASE_DIR.OUT_DEV}/img`));
  src([
    `${BASE_DIR.SRC}/svg/*.svg`,
  ]).pipe(dest(`${BASE_DIR.OUT_DEV}/svg`));
  cb();
}

function imgBuild(cb) {
  // image & svg assets
  src([
    `${BASE_DIR.SRC}/img/*.png`,
    `${BASE_DIR.SRC}/img/*.jpg`,
    `${BASE_DIR.SRC}/img/*.jpeg`,
  ]).pipe(dest(`${BASE_DIR.OUT}/img`));
  src([
    `${BASE_DIR.SRC}/svg/*.svg`,
  ]).pipe(dest(`${BASE_DIR.OUT}/svg`));
  cb();
}

function staticRootDev(cb) {
  // places everything from src/staticroot 
  // into root of dist static dir
  src([
    `${BASE_DIR.SRC}/staticroot/*`,
  ]).pipe(dest(`${BASE_DIR.OUT_DEV}/`));
  cb();
}

function staticRootBuild(cb) {
  // places everything from src/staticroot 
  // into root of dist static dir
  src([
    `${BASE_DIR.SRC}/staticroot/*`,
  ]).pipe(dest(`${BASE_DIR.OUT}/`));
  cb();
}


exports.build = series(cleanBuild, bundleBuild, parallel(cssBuild, jsBuild, imgBuild, staticRootBuild));

exports.dev = function() {
  cleanDev();
  watch([`${BASE_DIR.SRC}/**/*`], series(bundleDev, parallel(cssDev, jsDev, imgDev, staticRootDev)));
};
