/* -------------------------------------------------------------------------- */
/*                                 Toast Class                                */
/* -------------------------------------------------------------------------- */
class Toasts {
  constructor(tToRead = 2, tAnim = 0.5) {
    /**
     *  Messaging toast cards which pop up and then disappear after a
     *  short time.
     *  @param tToRead time in seconds each toast is expected to be read in
     *    (before it disappears)
     *  @param tAnim time in seconds for the fade in/out animations
     */
    this.tToRead = tToRead;
    this.tAnim = tAnim;
  }

  /** hides all toasts, beginning the hide animations */
  hideAll() {
    this.tl.reverse();
  }

  /** shows all toasts */
  showAll() {
    this.tl.play();
  }

  /** Selects and returns all toast elements on the DOM */
  static selectAllToasts(className = '.toast') {
    return Array.from(document.querySelectorAll(className));
  }

  genAnimationTimeline() {
    let tl = gsap.timeline({ paused: true });
    let nToasts = this.toasts.length;
    let tHide = this.tToRead * nToasts;
    let tStagger = this.tAnim / nToasts;
    tl.fromTo(
      this.toasts,
      { autoAlpha: 0, y: '-100%' },
      { autoAlpha: 1, stagger: tStagger, y: 0 }
    );
    // delay long enough for the user to read toasts
    tl.eventCallback('onComplete', () => {
      setTimeout(() => {
        // .. then begin hiding them.
        this.hideAll();
      }, tHide * 1000);
    });
    this.tl = tl;
  }

  /** Initialises any/all toasts on the DOM and begins their display */
  initAll() {
    this.toasts = Toasts.selectAllToasts();
    if (this.toasts.length > 0) {
      this.genAnimationTimeline();
      this.showAll();
    }
  }
}

export default Toasts;
