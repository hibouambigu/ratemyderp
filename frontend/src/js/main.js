/* -------------------------------------------------------------------------- */
/*                           RateMyDerp Main Scripts                          */
/* -------------------------------------------------------------------------- */

import '../sass/main.scss';

/* ----------------------------- Module Imports ----------------------------- */
import RatingSlider from './RatingSlider';
import Form from './Form';
import MainMenu from './MainMenu';
import DetailMenu from './DetailMenu';
import Toasts from './Toasts';

const MOBILE_MAX_WIDTH_PX = 600;

let mainMenu = new MainMenu(
  'main-menu',
  'main-menu-open',
  'main-menu-close',
  MOBILE_MAX_WIDTH_PX
);

let derpDetailMenu = new DetailMenu(
  'derp-detail-pane',
  'detail-pane-open',
  'detail-pane-close',
  MOBILE_MAX_WIDTH_PX
);
let form = new Form();

let ratingSlider = new RatingSlider('rating-slider');

let toasts = new Toasts();

/* ------------------------------ Document Load ----------------------------- */
window.addEventListener('DOMContentLoaded', () => {
  // init
  mainMenu.init();
  ratingSlider.init();
  form.init();
  if (document.getElementById('derp-detail-pane') != undefined)
    derpDetailMenu.init();
  toasts.initAll();
});
