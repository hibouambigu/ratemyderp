/* -------------------------------------------------------------------------- */
/*                          Various Utility Functions                         */
/* -------------------------------------------------------------------------- */

/** Convert REM units to pixels (can handle raw CSS "x.xrem" string) */
const remToPx = remVal =>
  remVal.match(/\d+[.]*\d*/g)[0] *
  parseFloat(getComputedStyle(document.documentElement).fontSize);

export { remToPx };
