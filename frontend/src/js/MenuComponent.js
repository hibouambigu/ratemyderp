/* -------------------------------------------------------------------------- */
/*                      Generic Animatable Menu Component                     */
/* -------------------------------------------------------------------------- */
/*
    An abstract class to extend, for keeping things DRY.
    Provides:
      - viewport "wide" detection for possible style changes driven by js
      - isOpen state and setter for opening/closing the Menu
*/

class MenuComponent {
  /**
   * @param wideViewportThresholdPx the threshold in pixels to define a wide viewport (equal to or greater than)
   */
  constructor(wideViewportThresholdPx) {
    this.wideViewportThresPx = wideViewportThresholdPx;
    this.isWideViewport = false;
    this.isOpenState = false;
    this.openBtn = this.closeBtn = this.toggleBtn = null;
  }

  /* ---------------------------- PUBLIC INTERFACE ---------------------------- */
  /** Called to initialise the Menu. Call once related DOM elements are ready. */
  init() {
    this.initElements();
    this.mountElements();
    this.attachHandlers();
    this._measureViewportType();
    window.addEventListener('resize', this._measureViewportType);
  }
  /** Called to open/close the Menu. Calls respective animation methods as well. */
  set isOpen(newState) {
    if (newState !== this.isOpenState) {
      this.isOpenState ? this.animateClose() : this.animateOpen();
      this.isOpenState = newState;
    }
  }
  get isOpen() {
    return this.isOpenState;
  }
  setOpenBtn(id) {
    let openBtn = document.getElementById(id);
    openBtn.addEventListener('click', () => {
      this.isOpen = true;
    });
  }
  setCloseBtn(id) {
    let closeBtn = document.getElementById(id);
    closeBtn.addEventListener('click', () => {
      this.isOpen = false;
    });
  }
  setToggleBtn(id) {
    let toggleBtn = document.getElementById(id);
    toggleBtn.addEventListener('click', () => {
      this.isOpen = !this.isOpen;
    });
  }

  /* ----------------------------- OVERRIDE THESE ----------------------------- */
  initElements() {}
  mountElements() {}
  attachHandlers() {}
  animateOpen() {}
  animateClose() {}

  /* ----------------------- IMPLEMENT THESE IF DESIRED ----------------------- */
  /** Called whenever the viewport type changes. Put anything you want to re-init or update after viewport change here. */
  viewportHasChanged() {}

  /* ---------------------- NOT INTENDED TO BE OVERRIDEN ---------------------- */
  /** Called automatically by window.onresize to determine current viewport type. */
  _measureViewportType = () => {
    let isWideNow = window.innerWidth >= this.wideViewportThresPx;
    if (isWideNow !== this.isWideViewport) {
      this.isWideViewport = isWideNow;
      this.viewportHasChanged();
    }
  };

  /** Called to check if elements exist on the DOM. Will skip component init() if not. */
}

export default MenuComponent;
