/* -------------------------------------------------------------------------- */
/*                             TextField Component                            */
/* -------------------------------------------------------------------------- */

import FormField from './FormField';

class TextField extends FormField {
  constructor(elem) {
    super(elem, false);
  }

  getInputElements() {
    // return this.elem.getElementsByClassName('input')[0];
    return this.elem.querySelector('input');
  }

  addListeners() {
    this.elem.addEventListener('input', e => {
      this.baseChangeHandler();
    });
  }
}

export default TextField;
