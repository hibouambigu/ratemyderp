/* -------------------------------------------------------------------------- */
/*                               Form Component                               */
/* -------------------------------------------------------------------------- */

import RadioField from './RadioField';
import TextField from './TextField';
import ImageField from './ImageField';

class Form {
  /** Attaches to any form elements on the page, initialises the individual
      elements (fields) and provides basic form validation */
  constructor() {
    this.fields = [];
    this.radioFields = [];
  }

  /** Called after DOM content loaded to init form elements */
  init() {
    this.form = document.getElementsByClassName('form')[0];
    this.genInputFields();
    this.initInputFields();
  }

  /**  Generates all FormFields as their correct FormField classes
   *    and populates fields[] with them */
  genInputFields() {
    // this.genTextFields();
    this.genRadioFields();
    this.genImageUploadField();
  }

  /** Initialise each field in fields[] */
  initInputFields = () => this.fields.forEach(field => field.init());

  // getAndPush methods for various FormField types
  genTextFields() {
    Array.from(document.getElementsByClassName('input__field--text')).forEach(
      elem => this.fields.push(new TextField(elem))
    );
  }
  genRadioFields() {
    Array.from(document.getElementsByClassName('input__field--radio')).forEach(
      elem => this.fields.push(new RadioField(elem))
    );
  }
  genImageUploadField() {
    // this.imgElem = document.getElementsByClassName('input__field--image')[0];
    // create filepond instance
    // this.filepond = FilePond.create(this.imgElem);
    Array.from(document.getElementsByClassName('input__field--image')).forEach(
      elem => this.fields.push(new ImageField(elem))
    );
  }
}

export default Form;
