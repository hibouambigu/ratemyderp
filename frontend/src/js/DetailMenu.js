/* -------------------------------------------------------------------------- */
/*                                 Detail Menu                                */
/* -------------------------------------------------------------------------- */

import MenuComponent from './MenuComponent';
import Backdrop from './Backdrop';

class DetailMenu extends MenuComponent {
  /**
   * A detail pane which shows/hides from a bar+button trigger on mobile. On
   * wider viewports it expands from the bar or is continually open.
   * @param {*} paneId id of the element to attach the pane to
   * @param {*} openId the button used to open the menu's id
   * @param {*} closeId button used to close the menu's id
   * @param {*} wideViewThresPx threshold in px for which a wide view is defined
   */
  constructor(paneId, openId, closeId, wideViewThresPx) {
    super(wideViewThresPx);
    this.paneId = paneId;
    this.openId = openId;
    this.closeId = closeId;
    this.tAnimate = 0.3;
    this.backdrop = new Backdrop('app-view-container', this.tAnimate);
  }

  initElements() {
    this.pane = document.getElementById(this.paneId);
    this.bar = this.pane.getElementsByClassName('detailpane__bar')[0];
    this.body = this.pane.getElementsByClassName('detailpane__body')[0];
    this.openBtn = document.getElementById(this.openId);
    this.closeBtn = document.getElementById(this.closeId);
    this._getPaneChildRows();
    this.backdrop.init(() => (this.isOpen = false));
    this.setOpenBtn(this.openId);
    this.setCloseBtn(this.closeId);
    this.genAnimTimeline();
  }

  animateOpen = () => {
    this.openTimeline.play();
    if (!this.isWideViewport) this.backdrop.show();
  };

  animateClose = () => {
    if (!this.isWideViewport) {
      this.openTimeline.reverse();
      this.backdrop.hide();
    }
  };

  genAnimTimeline() {
    let tOpen = this.tAnimate;
    let tlo = gsap.timeline({ paused: true });
    tlo.fromTo(this.body, { autoAlpha: 0 }, { autoAlpha: 1, duration: tOpen });
    tlo.fromTo(
      this.paneRows,
      { opacity: 0 },
      {
        opacity: 1,
        duration: tOpen,
      },
      '<'
    );
    tlo.fromTo(
      this.paneRows,
      { height: 0 },
      { height: 'auto', ease: 'elastic.out(1, 0.4)' },
      '<'
    );
    this.openTimeline = tlo;
  }

  viewportHasChanged() {
    if (this.isWideViewport) {
      this.backdrop.hide();
      this.isOpen = true;
    }
  }

  _getPaneChildRows() {
    this.paneRows = [].slice.call(this.body.children);
  }
}

export default DetailMenu;
