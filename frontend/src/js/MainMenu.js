/* -------------------------------------------------------------------------- */
/*                        Main Fullscreen & Mobile Menu                       */
/* -------------------------------------------------------------------------- */

import MenuComponent from './MenuComponent';
import Backdrop from './Backdrop';

class MainMenu extends MenuComponent {
  constructor(menuId, openId, closeId, wideViewThresPx) {
    super(wideViewThresPx);
    this.menuId = menuId;
    this.openId = openId;
    this.closeId = closeId;
    this.tAnimate = 0.3;
    this.backdrop = new Backdrop('app-backing-grid', this.tAnimate);
  }

  initElements() {
    this.menu = document.getElementById(this.menuId);
    this.openBtn = document.getElementById(this.openId);
    this.closeBtn = document.getElementById(this.closeId);
    this.setOpenBtn(this.openId);
    this.setCloseBtn(this.closeId);
    this.ease = CustomEase.create(
      'pulldown',
      'M0,0 C0.032,0.138 0.212,0.161 0.43,0.15 0.804,0.13 0.482,0.55 1,1 '
    );
    this.genPulldownTimeline();
    this.genOpenRightTimeline();
    this.backdrop.init(() => (this.isOpen = false));
  }

  genPulldownTimeline() {
    let pdtl = gsap.timeline({ paused: true });
    pdtl.fromTo(this.menu, { autoAlpha: 0 }, { autoAlpha: 1, duration: 0.01 });
    pdtl.fromTo(
      this.menu,
      { y: '-100%', left: 0 },
      {
        y: 0,
        left: 0,
        duration: 0.5,
        ease: 'pulldown',
      }
    );
    pdtl.to(this.menu, { y: 0, duration: 0.01 }); // buggy custom ease fix
    pdtl.fromTo(
      '#main-menu__top-row',
      { autoAlpha: 0, x: '100%' },
      {
        autoAlpha: 1,
        x: 0,
      }
    );
    pdtl.fromTo(
      '.menu-item',
      { autoAlpha: 0, x: '-100%' },
      {
        autoAlpha: 1,
        x: 0,
        stagger: 0.1,
      },
      '<'
    );
    this.pulldownTimeline = pdtl;
  }

  genOpenRightTimeline() {
    let ortl = gsap.timeline({ paused: true });
    ortl.fromTo(this.menu, { autoAlpha: 0 }, { autoAlpha: 1, duration: 0.01 });
    ortl.fromTo(
      this.menu,
      { left: '100%', y: 0 },
      { left: '45%', y: 0, duration: 0.4 }
    );
    ortl.fromTo(
      ['#main-menu__top-row', '.main-menu__titlebar', '.menu-item'],
      { autoAlpha: 0, x: '33%' },
      { autoAlpha: 1, x: 0, stagger: 0.05 }
    );
    this.openRightTimeline = ortl;
  }

  animateOpen() {
    this.timelineToUse().play();
    this.backdrop.show();
  }

  animateClose() {
    this.timelineToUse().reverse();
    this.backdrop.hide();
  }

  timelineToUse = () =>
    this.isWideViewport ? this.openRightTimeline : this.pulldownTimeline;

  viewportHasChanged() {
    if (this.isOpen) {
      // reset both timelines to paused at t=0
      this.openRightTimeline.pause(0);
      this.pulldownTimeline.pause(0);
      // now jump to the end in the new timeline's style
      this.timelineToUse().progress(1).pause();
    }
  }
}

export default MainMenu;
