/* -------------------------------------------------------------------------- */
/*                          FormField Component Class                         */
/* -------------------------------------------------------------------------- */
/* 
  A partially abstract class for different types of form fields to inherit
    from and be placed into a Form as.
*/
class FormField {
  /**
   * FormField is a base class for other field types to inherit from.
   * They are children of a Form object.
   * @param elem the field element (consisting of input, label and helper text)
   * @param labelShouldMove boolean to indicate whether the field label text
   *  should or should not animate above the input field when user content
   *  has been entered. Generally true for text type inputs, false for others.
   */
  constructor(elem, labelShouldMove = false) {
    this.elem = elem;
    this.id = elem.id;
    this.input = this.getInputElements();
    this.label = this.elem.getElementsByClassName('input__label')[0];
    this.helper = this.elem.getElementsByClassName('input__helper')[0];
    this.isValid = false;
    this.hasErrorState = false;
    this.labelShouldMove = labelShouldMove;
  }

  init(userContent = '') {
    this.addListeners();
    this.value = userContent; // set any preloaded user content
    if (this.labelShouldMove) this._moveLabelPosition();
    // run any user-specified init routines
    if (this.fieldDidInit !== undefined) this.fieldDidInit();
  }

  /* ----------------------- Override for Implementation ---------------------- */
  /**
   * Called during init to acquire input element(s). Could be a single
   *    element (eg: text) or an array (eg: radio selects)
   *    Should return an element or element list.
   */
  getInputElements() {
    console.log(
      'FormField: you probably want to define a getInputElements() method...'
    );
  }

  /** Any custom initialisation routines */
  fieldDidInit() {}

  /** Called during init to add listeners for user input */
  addListeners() {
    console.log(
      'FormField: you probably want to define an addListeners() method...'
    );
  }

  /** Should be called within any custom change handler implementation. */
  baseChangeHandler() {
    console.log('base change handler');
    if (this.labelShouldMove) this._moveLabelPosition();
  }

  get value() {
    return this.input.value;
  }
  set value(str) {
    this.input.value = str;
  }
  get hasUserContent() {
    return this.input.value !== '';
  }
  set helperText(str) {
    this.helper.innerHTML = str;
  }
  get helperText() {
    return this.helper.innerHTML;
  }
  /** Sets the FormField's error state, highlighting it if true */
  set hasError(hasError) {
    if (hasError != this.hasErrorState) {
      this._setIsErrorOnInput(hasError);
      this.hasErrorState = hasError;
    }
  }
  get hasError() {
    return this.hasErrorState;
  }

  /* ------------------------ Not Intended for Override ----------------------- */
  /** Sets or clears error classes on the FormField element */
  _setIsErrorOnInput(isError) {
    if (isError) this.elem.classList.add('input__field--is-error');
    else this.elem.classList.remove('input__field--is-error');
  }
  /** Moves label position by setting classes based on userContent
   * filling the input
   */
  _moveLabelPosition() {
    if (this.hasUserContent) {
      this.label.classList.add('input__label--is-above-input');
    } else {
      this.label.classList.remove('input__label--is-above-input');
    }
  }
}

export default FormField;
