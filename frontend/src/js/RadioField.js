/* -------------------------------------------------------------------------- */
/*                            RadioField Component                            */
/* -------------------------------------------------------------------------- */

import FormField from './FormField';

class RadioField extends FormField {
  constructor(elem) {
    super(elem, false);
  }

  getInputElements() {
    // return Array.from(this.elem.getElementsByClassName('radio'));
    return Array.from(this.elem.querySelectorAll('input[type=radio]'));
  }

  fieldDidInit() {
    // store label element and its base string (for modifying choice string)
    this.label = this.elem.getElementsByClassName('input__label')[0];
    this.labelBaseStr = this.label.innerHTML;
  }

  addListeners() {
    this.input.forEach(radio =>
      radio.addEventListener('change', e => {
        this.currentChoiceTextDisplay = this.currentChoiceFullText;
      })
    );
  }

  set value(str) {
    let checkVal = str.toLowerCase();
    this.input.forEach(radio => {
      radio.checked = radio.value.toLowerCase() === checkVal;
    });
  }

  get value() {
    return this.getCheckedRadio().value;
  }

  /** Return the current django choices full text for the selected choice */
  get currentChoiceFullText() {
    let choiceElem = this.getCheckedRadio().parentElement;
    return choiceElem !== undefined
      ? choiceElem.getElementsByClassName('species__choice-text')[0].innerText
      : '';
  }

  /** Sets chosen display text the specified string */
  set currentChoiceTextDisplay(choiceText) {
    this.label.innerHTML = `${this.labelBaseStr} - ${choiceText}`;
  }

  getCheckedRadio() {
    let checked = false;
    this.input.forEach(radio => {
      if (radio.checked) checked = radio;
    });
    return checked;
  }

  get hasUserContent() {
    return this.getCheckedRadio() ? true : false;
  }
}

export default RadioField;
