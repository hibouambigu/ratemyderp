/* -------------------------------------------------------------------------- */
/*                             Image Upload Field                             */
/* -------------------------------------------------------------------------- */

// filepond
import FormField from './FormField';
import * as FilePond from 'filepond';
import 'filepond/dist/filepond.min.css';
// plugins for filepond
import FilePondPluginImageResize from 'filepond-plugin-image-resize';
import FilePondPluginImageTransform from 'filepond-plugin-image-transform';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginImageCrop from 'filepond-plugin-image-crop';
import FilePondPluginImageValidateSize from 'filepond-plugin-image-validate-size';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
FilePond.registerPlugin(
  FilePondPluginImagePreview,
  FilePondPluginImageResize,
  FilePondPluginImageTransform,
  FilePondPluginImageCrop,
  FilePondPluginImageValidateSize,
  FilePondPluginFileValidateType
);

// consts
const IDEAL_IMAGE_WIDTH = 1080;

class ImageField extends FormField {
  constructor(elem) {
    super(elem, false);
  }

  getInputElements() {
    return document.getElementsByClassName('image-upload')[0];
  }

  fieldDidInit() {
    // FilePond configuration
    const fpConf = {
      // cropping
      imageCropAspectRatio: '1:1',
      // resizing
      imageResizeTargetHeight: IDEAL_IMAGE_WIDTH,
      imageResizeTargetWidth: IDEAL_IMAGE_WIDTH,
      imageResizeUpscale: false,
      // general
      dropValidation: true,
      maxFiles: 1,
      maxParallelUploads: 1,
      storeAsFile: true,
      // labels/text
      labelIdle:
        'Drop or <span class="filepond--label-action">browse</span> for an image.',
      credits: false,
      // image validation
      acceptedFileTypes: ['image/png', 'image/jpeg', 'image/jpg'],
      labelFileTypeNotAllowed: 'Invalid image file type!',
    };
    // instantiate FilePond
    this.fp = FilePond.create(this.input, fpConf);
  }

  addListeners() {}

  /** Loads an image into the ImageField
   * @param fileUrl filepath of the image to load
   */
  set value(fileUrl) {
    if (fileUrl != '' && fileUrl != null)
      this.fp
        .addFile(fileUrl, { index: 0 })
        .then(file => {})
        .catch(error => {
          console.log(error);
        });
  }
  /** Returns the filename of the presently loaded image file. False if nothing loaded. */
  get value() {
    if (this.hasUserContent) {
      return this.fp.getFile(0).filename;
    } else {
      return false;
    }
  }

  /** Queries whether there is an image loaded into the field or not */
  get hasUserContent() {
    return this.fp.getFile(0) != null && true;
  }
}

export default ImageField;
