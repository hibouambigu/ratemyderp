/* -------------------------------------------------------------------------- */
/*                             Derp Rating Slider                             */
/* -------------------------------------------------------------------------- */

import { remToPx } from './utils';

const COLOR_STOPS = [
  '#27d0ff',
  '#48bcec',
  '#5eafdf',
  '#72a3d3',
  '#8796c7',
  '#9c8abb',
  '#b17daf',
  '#c670a3',
  '#dc6397',
  '#fc5185',
];

class RatingSlider {
  constructor(id) {
    this.groupId = id;
  }

  init() {
    if ((this.group = document.getElementById(this.groupId)) != null) {
      this.initElements();
      this.awaitWholePageLoad();
      this.updateScale();
      this.refreshDisplayValue();
    }
  }

  /** Executes when entire page loaded; avoids elem.clientWidth browser bugs */
  awaitWholePageLoad() {
    window.scrollBy(0, 1); // make it work in Safari
    window.addEventListener('load', () => {
      this.updateScale();
    });
  }

  initElements() {
    this.input = this.group.getElementsByClassName('rating__input')[0];
    this.value = this.group.getElementsByClassName('rating__value')[0];
    this.scale = this.group.getElementsByClassName('rating__scale')[0];
    this.input.addEventListener('input', this.changeValue);
    window.onresize = this.updateScale;
  }

  refreshDisplayValue = () => this.input.dispatchEvent(new Event('input'));

  appendTickMarks(e) {
    for (let n = 0; n < this.nTicks; n++) {
      e.append(this.genTickElement(n));
    }
  }

  appendExtrema(e) {
    let minLabel = document.createElement('div');
    minLabel.classList.add('rating__extrema', 'rating__extrema--min');
    minLabel.style.left = `${this.tickLeftPxFromPos(0)}px`;
    minLabel.innerHTML = this.input.min;
    let maxLabel = document.createElement('div');
    maxLabel.classList.add('rating__extrema', 'rating__extrema--max');
    maxLabel.innerHTML = this.nTicks;
    maxLabel.style.left = `${this.tickLeftPxFromPos(this.nTicks - 1)}px`;
    e.append(minLabel, maxLabel);
  }

  changeValue = e => {
    let newVal = e.target.value;
    let newLeft = this.tickLeftPxFromPos(newVal - 1);
    this.value.style.left = `${newLeft}px`;
    this.value.innerHTML = `${newVal}`;
    this.input.style.setProperty('--thumb-color', `${COLOR_STOPS[newVal - 1]}`);
  };

  genTickElement = posN => {
    let tick = document.createElement('div');
    tick.classList.add('rating__tick');
    tick.style.left = `${this.tickLeftPxFromPos(posN)}px`;
    return tick;
  };

  tickLeftPxFromPos = posN => {
    // const thumbW = 45;
    let offset = this.thumbW / 2;
    let dX = (this.w - 2 * offset) / (this.nTicks - 1);
    return offset + posN * dX;
  };

  initProps() {
    this.w = this.input.clientWidth;
    this.nTicks = this.input.max;
    this.thumbW = remToPx(
      getComputedStyle(this.group).getPropertyValue('--thumb-h')
    );
  }

  /** Re-renders the scale component -- call after resize event */
  updateScale = () => {
    this.scale.textContent = ''; // destroy children
    this.initProps();
    this.appendTickMarks(this.scale);
    this.appendExtrema(this.scale);
    this.refreshDisplayValue();
  };
}

export default RatingSlider;
