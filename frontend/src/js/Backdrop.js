/* -------------------------------------------------------------------------- */
/*                             Backdrop Component                             */
/* -------------------------------------------------------------------------- */
class Backdrop {
  /**
      Placed behind menus, modals etc to darken the app when an overlay is
        placed on top of it, and to provide click handling for clicking
        out of said UI components.
  */
  constructor(parentId, tAnimate) {
    this.tAnimate = tAnimate;
    this.parentId = parentId;
  }

  init(handleClickFn) {
    this.parent = document.getElementById(this.parentId);
    this.backdrop = this._genBackdrop();
    this.parent.appendChild(this.backdrop);
    this.parent.style.position = 'relative'; // backdrop relies on absolute positioning
    if (handleClickFn !== undefined)
      this.backdrop.addEventListener('click', () => handleClickFn());
  }

  show() {
    gsap.fromTo(
      this.backdrop,
      { autoAlpha: 0 },
      { autoAlpha: 1, duration: this.tAnimate }
    );
  }

  hide() {
    gsap.to(this.backdrop, { autoAlpha: 0, duration: this.tAnimate });
  }

  _genBackdrop = () => {
    let bd = document.createElement('div');
    bd.classList.add('backdrop', 'backdrop--is-dimmed');
    return bd;
  };
}

export default Backdrop;
