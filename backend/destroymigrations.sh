#!/bin/bash
echo -e "Deleting migrations and wiping postgres db..."
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete &&
find . -path "*/migrations/*.pyc"  -delete &&
docker-compose down --volumes