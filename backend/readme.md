# Build / Test

## Testing

Run tests
`coverage run -m pytest`

## Development

1. Don't forget to define and source local .env settings ;) Should contain env vars for connection to postgres, redis etc
2. Celery beat & worker consolidated run command: `celery -A config.celery_app worker -B --loglevel=info` -> not for production use.
