"""
Testing runs faster with these settings.
"""

from .base import *  # noqa
from .base import env

TEST_CONF = True

SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="ErFo6oHTyzIxBX6kBGMpsVSwJWUyXuv3a5F7bEApjmzAJ5HIwGoV7wO2sLJAwQkD",
)
TEST_RUNNER = "django.test.runner.DiscoverRunner"


# ---------------------------------- CACHING --------------------------------- #
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}


# ---------------------------------- HASHING --------------------------------- #
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# ----------------------------------- EMAIL ---------------------------------- #
EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"

# --------------------------------- TEMPLATES -------------------------------- #
TEMPLATES[-1]["OPTIONS"]["loaders"] = [  # type: ignore[index] # noqa: F405
    (
        "django.template.loaders.cached.Loader",
        [
            "django.template.loaders.filesystem.Loader",
            "django.template.loaders.app_directories.Loader",
        ],
    )
]
