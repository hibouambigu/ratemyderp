from .base import *  # noqa
from .base import env

# ---------------------------------- GENERAL --------------------------------- #
DEBUG = True
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="django-insecure-bcytm#+b0*-!_)lq7h$yc*(byl^tee$eplbu--+=j6^3rlcnxh",
)
ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
    "10.69.66.169",
    "ratemyderp.com",
]

# ---------------------------------- CACHING --------------------------------- #
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}

# -------------------------- MAILHOG LOCAL DEV EMAIL ------------------------- #
EMAIL_HOST = "localhost"
EMAIL_PORT = 1025

# ------------------------------- DEBUG TOOLBAR ------------------------------ #
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
INSTALLED_APPS += ["debug_toolbar"]  # noqa F405
MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]  # noqa F405
DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel"],
    "SHOW_TEMPLATE_CONTEXT": True,
}
INTERNAL_IPS = ["127.0.0.1", "10.0.2.2", "10.69.66.169"]


# ----------------------------- DJANGO EXTENSIONS ---------------------------- #
# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration
INSTALLED_APPS += ["django_extensions"]  # noqa F405

# ---------------------------------- CELERY ---------------------------------- #
CELERY_TASK_ALWAYS_EAGER = False  # enable when not running broker in dev
CELERY_TASK_EAGER_PROPAGATES = True  # exceptions will propagate from ^^
