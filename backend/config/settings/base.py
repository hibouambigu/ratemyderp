# ---------------------------------------------------------------------------- #
#                          Rate My Derp! Base Settings                         #
# ---------------------------------------------------------------------------- #
"""
Base settings file. Dev/Production modes extend this file.
"""
from pathlib import Path
from telnetlib import AUTHENTICATION

import environ

ROOT_DIR = Path(__file__).resolve(strict=True).parent.parent.parent
# ratemyderp/
APPS_DIR = ROOT_DIR / "ratemyderp"
env = environ.Env()

READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=False)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(ROOT_DIR / ".env"))

# ---------------------------------- GENERAL --------------------------------- #
DEBUG = env.bool("DJANGO_DEBUG", False)
TEST_CONF = False
TIME_ZONE = "America/New_York"
LANGUAGE_CODE = "en-us"
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = [str(ROOT_DIR / "locale")]

# --------------------------------- DATABASES -------------------------------- #
DATABASES = {
    "default": env.db("DATABASE_URL", default="postgres:///ratemyderp"),
}
DATABASES["default"]["ATOMIC_REQUESTS"] = True
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# ----------------------------------- URLs ----------------------------------- #
ROOT_URLCONF = "config.urls"
WSGI_APPLICATION = "config.wsgi.application"

# ----------------------------------- APPS ----------------------------------- #
DJANGO_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "django.forms",
]
THIRD_PARTY_APPS = ["allauth", "allauth.account", "imagekit"]
LOCAL_APPS = [
    "ratemyderp.users",
    "ratemyderp.pages",
    "ratemyderp.derps",
    "ratemyderp.rating",
    "ratemyderp.utils",
    "ratemyderp.profiles",
    "ratemyderp.stats",
    "django.contrib.admin",
]
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# -------------------------------- MIGRATIONS -------------------------------- #
# MIGRATION_MODULES = {"sites": "ratemyderp.contrib.sites.migrations"}

# ------------------------------ AUTHENTICATION ------------------------------ #
AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
)
AUTH_USER_MODEL = "users.DerpUser"
LOGIN_REDIRECT_URL = "go-rate"
LOGIN_URL = "account_login"

# --------------------------------- PASSWORDS -------------------------------- #
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# -------------------------------- MIDDLEWARE -------------------------------- #
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.common.BrokenLinkEmailsMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

# ------------------------------- STATIC FILES ------------------------------- #
STATIC_ROOT = str(ROOT_DIR / "dist")
STATIC_URL = "/static/"
STATICFILES_DIRS = [str(APPS_DIR / "static")]
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# -------------------------------- MEDIA FILES ------------------------------- #
MEDIA_ROOT = str(APPS_DIR / "media")
MEDIA_URL = "/media/"

# --------------------------------- TEMPLATES -------------------------------- #
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [str(APPS_DIR.joinpath("templates"))],
        "OPTIONS": {
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                # "ratemyderp.utils.context_processors.settings_context",
                "ratemyderp.utils.context_processors.base_url_context_processor",
            ],
        },
    }
]

# https://docs.djangoproject.com/en/dev/ref/settings/#form-renderer
FORM_RENDERER = "django.forms.renderers.TemplatesSetting"

# --------------------------------- FIXTURES --------------------------------- #
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
FIXTURE_DIRS = (str(APPS_DIR / "fixtures"),)

# --------------------------------- SECURITY --------------------------------- #
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
X_FRAME_OPTIONS = "DENY"

CSRF_TRUSTED_ORIGINS = ["https://ratemyderp.com", "http://ratemyderp.com"]

# ----------------------------------- EMAIL ---------------------------------- #
EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND",
    default="django.core.mail.backends.smtp.EmailBackend",
)
EMAIL_TIMEOUT = 5

# ----------------------------------- ADMIN ---------------------------------- #
ADMIN_URL = env("DJANGO_ADMIN_URL", default="admin/")
ADMINS = [("""Rate My Derp""", "ratemyderp@gmail.com")]
MANAGERS = ADMINS

# ---------------------------------- LOGGING --------------------------------- #
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "root": {"level": "INFO", "handlers": ["console"]},
}

# ---------------------------- DJANGO-ALLAUTH-CONF --------------------------- #
# ACCOUNT_ALLOW_REGISTRATION = env.bool("DJANGO_ACCOUNT_ALLOW_REGISTRATION", True)
# # https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_AUTHENTICATION_METHOD = "username_email"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_VERIFICATION = "mandatory"
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False
# ACCOUNT_ADAPTER = "ratemyderp.users.adapters.AccountAdapter"
# SOCIALACCOUNT_ADAPTER = "ratemyderp.users.adapters.SocialAccountAdapter"
# # https://django-allauth.readthedocs.io/en/latest/forms.html
# ACCOUNT_FORMS = {"signup": "ratemyderp.users.forms.UserSignupForm"}
# SOCIALACCOUNT_FORMS = {"signup": "ratemyderp.users.forms.UserSocialSignupForm"}

# --------------------------------- MESSAGING -------------------------------- #
# set the classes applied for each message level
from django.contrib.messages import constants as messages

MESSAGE_TAGS = {
    messages.ERROR: "is-error",
    messages.SUCCESS: "is-tertiary",
    messages.INFO: "is-secondary",
    messages.WARNING: "is-warning",
}

# ---------------------------------- CELERY ---------------------------------- #
if USE_TZ:
    CELERY_TIMEZONE = TIME_ZONE
CELERY_BROKER_URL = env("CELERY_BROKER_URL")
CELERY_RESULT_BACKEND = CELERY_BROKER_URL
# https://docs.celeryproject.org/en/latest/userguide/calling.html#calling-serializers
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
# https://docs.celeryproject.org/en/latest/userguide/configuration.html#task-time-limit
CELERY_TASK_TIME_LIMIT = 5 * 60
CELERY_TASK_SOFT_TIME_LIMIT = 60

# ------------------------------ PERIODIC TASKS ------------------------------ #
TASK_UPDATE_RANK_INTERVAL = 10
"interval in seconds between Derp rank stat calculation updates"
TASK_TIMESERIES_MEASUREMENT_INTERVAL = 20
"time to wait between timeseries measurements and subsequent writing to influxdb"

# --------------------------------- INFLUXDB --------------------------------- #
INFLUX_TOKEN = env("INFLUX_TOKEN")
INFLUX_ORG = env("INFLUX_ORG")
INFLUX_BUCKET = env("INFLUX_BUCKET")
INFLUX_URL = env("INFLUX_URL")
