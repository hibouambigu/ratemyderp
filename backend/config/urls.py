from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.views import defaults as default_views

urlpatterns = [
    # admin
    path(settings.ADMIN_URL, admin.site.urls),
    # user and account mgmt
    path("user/", include("ratemyderp.users.urls")),
    path("accounts/", include("allauth.urls")),
    # ratemyderp apps
    path("", include("ratemyderp.pages.urls")),
    path("derp/", include("ratemyderp.derps.urls")),
    path("rate/", include("ratemyderp.rating.urls")),
    path("profile/", include("ratemyderp.profiles.urls")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [
            path("__debug__/", include(debug_toolbar.urls))
        ] + urlpatterns
