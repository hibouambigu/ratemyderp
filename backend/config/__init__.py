# make sure celery app is loaded whenever django starts
from config.celery_app import app as celery_app

__all__ = ("celery_app",)
