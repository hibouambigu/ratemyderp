from django.shortcuts import redirect
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.views import View
from django.views.generic.base import RedirectView

from ratemyderp.derps.models import Derp
from ratemyderp.rating.models import DerpRating
from ratemyderp.utils.sessions import UserOrAnon
from ratemyderp.rating.forms import RatingForm

User = get_user_model()


class GoRatingView(RedirectView):
    """
    The main "Go Rating" app view.
    Presents the user with a Derp to rate and will only generate
    a fresh one to see if they vote on the currently displayed one.
    """

    def get_redirect_url(self, *args, **kwargs):
        user = UserOrAnon.from_request(self.request)
        derp_to_rate = (
            Derp.get_fresh_or_none(user)
            if user.get_next_derp_id_to_rate() == None
            else Derp.get_by_uuid(user.get_next_derp_id_to_rate())
        )
        self.url = (
            derp_to_rate.get_absolute_url()
            if derp_to_rate is not None
            else "/noderps"
        )
        return super().get_redirect_url(*args, **kwargs)


go_rating_view = GoRatingView.as_view()


class FreshRandomDerpView(RedirectView):
    """
    Redirects user to a new randomly selected derp, or to the noderps
    page if the user has exhausted all fresh derps to vote on
    """

    def get_redirect_url(self, *args, **kwargs):
        user = UserOrAnon.from_request(self.request)
        fresh_derp = Derp.get_fresh_or_none(user)
        self.url = (
            fresh_derp.get_absolute_url()
            if fresh_derp is not None
            else "/noderps"
        )
        return super().get_redirect_url(*args, **kwargs)


fresh_random_derp_view = FreshRandomDerpView.as_view()


class RateDerpView(View):
    """
    Handles GET or POST from user to rate derps.
    Presents the user with a new derp to rate only if they
    have just completed rating the one presented. This is done
    by returning fresh_random_derp_view when a rating is received.
    """

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.derp = Derp.get_by_uuid(kwargs["derp_uuid"])

    def get(self, request, *args, **kwargs):
        return redirect(self.derp.get_absolute_url())

    def post(self, request, *args, **kwargs):
        form = RatingForm(request.POST)
        # we're just using the form for validation since if the form is invalid,
        #   it means someone is likely trying to hack ratings. Ignore it.
        if form.is_valid():
            rating_value = request.POST["value"]
            # handle both anonymous session and authenticated users
            user = UserOrAnon.from_request(request)
            new_rating = DerpRating.create_or_update(
                user, self.derp, rating_value
            )
            if not user.is_anon:
                user.obj.profile.increment_n_derps_rated()
            return fresh_random_derp_view(self.request)
        else:
            # rating is invalid/has been tampered with
            return redirect(self.derp.get_absolute_url())


rate_derp_view = RateDerpView.as_view()


# --------------------------- Vote for Derp Rating --------------------------- #
# def rate_view(request, derp_uuid):
#   """
#   User is casting a new vote for a Derp rating.
#     - Validates the vote and casts it to the rating, updating the Derp
#       in the process
#     - Redirects the user to a new random derp from the db (to-do: pick from
#       Derp objects the user has not yet voted on)
#   """
#   derp = get_object_or_404(Derp, uuid=derp_uuid)
#   if request.method == 'POST':
#     rating = request.POST['rating']
#     # check for existing vote
#     vote = Vote.get_existing_or_none(derp, request)
#     if vote is None:
#       # create a new vote
#       if request.user.is_authenticated:
#         # User vote:
#         vote = Vote (
#           user=request.user, is_anonymous=False,
#           derp=get_object_or_404(Derp, uuid=derp_uuid),
#           weight=1, rating=rating
#         )
#       else:
#         # anon user session_id vote:
#         vote = Vote (
#           user = None, is_anonymous=True,
#           session_id=get_or_create_anon_id(request),
#           derp=get_object_or_404(Derp, uuid=derp_uuid),
#           weight=1, rating=rating
#         )
#     else:
#       # remove existing vote's influence
#       vote.remove()
#       # now update it
#       vote.rating = int(rating)
#     # save to db
#     vote.save()
#     # apply vote to Derp
#     vote.apply()
#     # TODO: once user profiles are implemented
#     # increment_user_vote_count(request.user)
#     # let the user know they voted
#     messages.success(request, f'{ derp.name } thanks you!')
#     # redirect to a new randomly selected derp, or to the noderps
#     # page if the user has exhausted all fresh derps to vote on
#     random_derp = Derp.get_fresh_or_none(request)
#     if random_derp is not None:
#       return redirect(f"/derp/{str(random_derp.uuid)}")
#     else:
#       return redirect('noderps')
#   else:
#     # GET request, so just return to the derp's page.
#     return redirect(f"/derp/{str(derp.uuid)}")
