# ---------------------------------------------------------------------------- #
#                     Model Behaviours (mixins) for Rating                     #
# ---------------------------------------------------------------------------- #

from django.db import models
from django.contrib.auth import get_user_model
from model_utils.models import TimeStampedModel

User = get_user_model()


class Rateable(TimeStampedModel):
    """
    Adds rating functionality to a child model.
    Calculate, store and present the average rating based on Rating votes
    cast by users.
    """

    value = models.FloatField(default=0, blank=True, editable=False)
    """the cumulative average rating at this point in time"""
    n_votes = models.IntegerField(default=0, editable=False)
    """total number of rating votes cast"""
    acc = models.IntegerField(default=0, editable=False)
    """accumulator for rating calculations"""

    def apply_vote(self, rating, weight):
        """
        Apply a vote to the Rating, generically.
        Sign of weight implies add/subtract influence to rating.
        """
        # 1. add rating to accumulator and increment count
        n_votes = self.n_votes + (1 if weight > 0 else -1)
        n_votes = 0 if n_votes < 0 else n_votes
        vote_acc = self.acc + int(rating) * weight
        if vote_acc < 0:
            vote_acc = 0
        # 2. compute new rating
        new_rating = 0 if n_votes == 0 else (float(vote_acc) / n_votes)
        # 3. update & save the Derp
        self.n_votes = n_votes
        self.acc = vote_acc
        self.value = new_rating
        self.save()
        return new_rating

    def add_vote(self, rating, weight=1):
        """
        Adds a vote's influence to the Rating calculation.
        """
        return self.apply_vote(rating, weight=abs(weight))

    def subtract_vote(self, rating, weight=1):
        """
        Subtracts a vote's influence from the Rating calculation.
        """
        return self.apply_vote(rating, weight=-abs(weight))
