from django.forms import ModelForm, NumberInput

from ratemyderp.rating.models import DerpRating


class RatingForm(ModelForm):
    class Meta:
        model = DerpRating
        fields = [
            "value",
        ]
        widgets = {
            "value": NumberInput(
                attrs={"type": "range", "step": "1", "min": "1", "max": "10"}
            )
        }
