from django.contrib import admin

from ratemyderp.rating.models import DerpRating


@admin.register(DerpRating)
class DerpRatingAdmin(admin.ModelAdmin):
    # columns to display
    list_display = (
        "created",
        "derp",
        "value",
        "user",
        "is_anonymous",
        "session_id",
    )
    list_filter = ("derp", "user", "weight", "is_anonymous")
    search_fields = ("derp", "user", "session_id", "is_anonymous")

    list_per_page = 50
