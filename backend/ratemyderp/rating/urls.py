from django.urls import path
from django.conf import settings

from ratemyderp.rating.views import rate_derp_view, fresh_random_derp_view

app_name = "rating"
urlpatterns = [
    path(route="<uuid:derp_uuid>", view=rate_derp_view, name="rate"),
]

if settings.TEST_CONF:
    # only create URL route for fresh derp for testing
    urlpatterns += [
        path(route="fresh", view=fresh_random_derp_view, name="fresh"),
    ]
