from django.core.exceptions import ValidationError


def validate_rating(value):
    """
    Validates rating is an integer from 1 to 10
    """
    if not 1 <= value <= 10:
        msg = "Must be from 1 to 10"
        raise ValidationError(msg)
