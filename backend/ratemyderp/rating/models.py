from django.db import models
from django.contrib.auth import get_user_model
from model_utils.models import TimeStampedModel

from ratemyderp.derps.models import Derp
from ratemyderp.utils.sessions import UserOrAnon
from ratemyderp.rating.validators import validate_rating

User = get_user_model()


class DerpRating(TimeStampedModel):
    """
    Rating for a Derp by a User.
    Used in the calculation of a given Derp's rating.
    - If the user is anonymous (is_anonymous flag) then we'll store their
      session's UUID instead of an authenticated user object.
    """

    derp = models.ForeignKey(Derp, on_delete=models.CASCADE)
    """the derp this rating is for"""
    user = models.ForeignKey(
        User, on_delete=models.SET_DEFAULT, default=None, blank=True, null=True
    )
    """registered user who cast the vote"""
    value = models.IntegerField(default=0, validators=[validate_rating])
    """point rating given"""
    weight = models.IntegerField(default=1, editable=False)
    """weighting"""
    is_anonymous = models.BooleanField(default=False, editable=False)
    """flag to identify an anonymous rating"""
    session_id = models.CharField(
        max_length=200, default="None", editable=False, blank=True
    )
    """anonymous rating session id"""

    @property
    def username(self):
        return "Anon" if self.is_anonymous else self.user.username

    def __str__(self):
        return f"{self.derp.name} by {self.username}"

    @classmethod
    def as_user(cls, user, derp, value, weight=1):
        return cls(user=user, derp=derp, value=value, weight=weight)

    @classmethod
    def as_anon(cls, session_id, derp, value, weight=1):
        return cls(
            is_anonymous=True,
            session_id=session_id,
            derp=derp,
            value=value,
            weight=weight,
        )

    def apply(self):
        """
        Applies this Rating's influence to its associated Derp.
        """
        self.derp.add_rating(self.value, self.weight)

    def remove(self):
        """
        Removes this Rating's influence from its associated Derp.
        """
        self.derp.subtract_rating(self.value, self.weight)

    @classmethod
    def get_existing_or_none(cls, user_or_anon, derp):
        """
        Query existing DerpRating from db - if it exists - else, None.
        """
        rating = None
        if isinstance(user_or_anon, User):
            rating = cls.objects.filter(user=user_or_anon, derp=derp).first()
        else:
            # check for anonymous rating
            rating = cls.objects.filter(
                session_id=user_or_anon, derp=derp
            ).first()
        return rating

    def overwrite(self, new_rating):
        """
        Overwrites this existing rating with a new rating value.
        """
        self.remove()
        self.value = int(new_rating)
        self.save()
        self.apply()

    @classmethod
    def create_or_update(cls, user_or_anon, derp, rating_value):
        """
        Creates a new Rating for the given user(or anon) and derp.
        Updates existing Rating via overwrite if it already exists.
        Used in views which create Ratings.
        Returns the new or existing rating after db creation/update.
        """
        UserOrAnon.assert_param_type(user_or_anon)

        existing_rating = cls.get_existing_or_none(user_or_anon.obj, derp)
        if existing_rating is None:
            rating_type = cls.as_anon if user_or_anon.is_anon else cls.as_user
            new_rating = rating_type(user_or_anon.obj, derp, rating_value)
            # save to db and apply
            new_rating.save()
            new_rating.apply()
            return new_rating
        else:
            existing_rating.overwrite(rating_value)
            return existing_rating
