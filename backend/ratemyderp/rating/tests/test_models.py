import pytest

from .factories import (
    DerpRatingFactory,
    UserDerpRatingFactory,
    SessionDerpRatingFactory,
)
from ..models import DerpRating
from ratemyderp.utils.sessions import UserOrAnon

pytestmark = pytest.mark.django_db


class TestDerpRatingModel:
    def test_username_str(self, anon_rating, user_rating):
        assert anon_rating.username == "Anon"
        assert user_rating.username == user_rating.user.username

    def test_to_str(self, anon_rating, user_rating):
        assert anon_rating.__str__() == f"{str(anon_rating.derp.name)} by Anon"
        assert (
            user_rating.__str__()
            == f"{str(user_rating.derp.name)} by {user_rating.username}"
        )


class TestDerpRatingAffectsDerp:
    def test_apply_rating(self, user_rating):
        user_rating.apply()
        derp = user_rating.derp
        assert (
            derp.rating_n_votes == 1
            and derp.rating_val == user_rating.value
            and derp.rating_acc == user_rating.value
        )

    def test_remove_rating(self, user_rating):
        # manually add rating:
        derp = user_rating.derp
        derp.rating_acc = user_rating.value
        derp.rating_val = user_rating.value
        derp.rating_n_votes = 1
        # now remove it:
        user_rating.remove()
        assert derp.rating_val == 0
        assert derp.rating_n_votes == 0
        assert derp.rating_acc == 0


class TestDerpRatingModelFactories:
    def test_from_user(self, derp, user):
        rating = DerpRating.as_user(user, derp, 5, 1)
        assert (
            rating.user is not None
            and rating.is_anonymous == False
            and rating.session_id == "None"
        )

    def test_from_anonymous(self, derp):
        anon_id = SessionDerpRatingFactory().session_id
        rating = DerpRating.as_anon(anon_id, derp, 5, 1)
        assert (
            rating.session_id == anon_id
            and rating.is_anonymous == True
            and rating.user == None
        )


class TestDerpRatingModelQueries:
    def test_get_existing_or_none_as_user(self, rf, user_rating):
        q = DerpRating.get_existing_or_none(user_rating.user, user_rating.derp)
        assert q == user_rating

    def test_get_existing_or_none_as_anon(self, derp, client):
        anon_id = client.session.session_key
        anon_rating = DerpRating(
            is_anonymous=True,
            session_id=anon_id,
            derp=derp,
            value=5,
        )
        anon_rating.save()
        q = DerpRating.get_existing_or_none(
            anon_rating.session_id, anon_rating.derp
        )
        assert q == anon_rating


class TestDerpRatingModelMethods:
    def test_overwrite(self, user_rating):
        old = user_rating.value
        new = old + 1 if old < 10 else 1
        derp = user_rating.derp
        derp.rating_val = old
        derp.rating_acc = old
        derp.rating_n_votes = 1
        user_rating.overwrite(new)
        assert derp.rating_val == new


class TestDerpRatingModelCreateOrUpdate:
    def test_user_new(self, user, derp):
        u = UserOrAnon(user)
        rating = DerpRating.create_or_update(u, derp, 9)
        assert rating is not None
        assert rating.is_anonymous == False

    def test_anon_new(self, derp, client):
        anon_id = client.session.session_key
        u = UserOrAnon(anon_id)
        rating = DerpRating.create_or_update(u, derp, 9)
        assert rating is not None
        assert rating.is_anonymous == True

    def test_anon_update(self, derp, client):
        anon_id = client.session.session_key
        u = UserOrAnon(anon_id)
        rating = DerpRating.create_or_update(u, derp, 9)
        assert rating.derp.rating_val == 9
        rating2 = DerpRating.create_or_update(u, derp, 8)
        assert rating2.derp.rating_val == 8

    def test_user_update(self, user, derp):
        u = UserOrAnon(user)
        rating = DerpRating.create_or_update(u, derp, 9)
        assert rating.derp.rating_val == 9
        rating2 = DerpRating.create_or_update(u, derp, 8)
        assert rating2.derp.rating_val == 8

    def test_not_instance_throws_exception(self, user, derp):
        u = user
        with pytest.raises(Exception):
            DerpRating.create_or_update(u, derp, 9)
