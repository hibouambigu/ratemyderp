import pytest
from django.core.exceptions import ValidationError

from ratemyderp.rating.validators import validate_rating


class TestValidateRating:
    def test_good_input(self):
        try:
            validate_rating(5)
        except Exception as ex:
            assert False, f"Exception was raised: {ex}"

    def test_too_low(self):
        with pytest.raises(ValidationError):
            validate_rating(0.0)

    def test_too_high(self):
        with pytest.raises(ValidationError):
            validate_rating(12)

    def test_good_but_strange_input(self):
        try:
            validate_rating(9.000)
        except Exception as ex:
            assert False, f"Exception was raised: {ex}"
