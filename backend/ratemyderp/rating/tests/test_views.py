import re
import pytest
from pytest_django.asserts import assertContains
from django.urls import reverse

from ratemyderp.rating.views import rate_derp_view, fresh_random_derp_view
from ratemyderp.derps.tests.factories import DerpFactory
from ratemyderp.profiles.tests.factories import UserProfileFactory


pytestmark = pytest.mark.django_db


class TestFreshRandomDerpView:
    def test_response_redirects(self, rf, user):
        req = rf.get(reverse("rating:fresh"))
        req.user = user
        res = fresh_random_derp_view(req)
        assert res.status_code == 302

    def test_returns_fresh_derp_for_user(self, client):
        d1 = DerpFactory()
        res = client.get(reverse("rating:fresh"), follow=True)
        assertContains(res, d1.name)

    def test_returns_noderps(self, client):
        res = client.get(reverse("rating:fresh"), follow=True)
        assertContains(res, "No Derps Left!")


class TestRateDerpView:
    def test_response_redirects(self, rf, derp, user):
        req = rf.get(reverse("rating:rate", kwargs={"derp_uuid": derp.uuid}))
        req.user = user
        res = rate_derp_view(req, derp_uuid=derp.uuid)
        assert res.status_code == 302

    def test_get_displays_derp(self, derp, client):
        res = client.get(
            reverse("rating:rate", kwargs={"derp_uuid": derp.uuid}), follow=True
        )
        assertContains(res, derp.name)

    def test_user_can_rate(self, derp, user, client):
        client.force_login(user)
        derp2 = DerpFactory()
        derp2.save()
        form_data = {
            "value": "10",
        }
        res = client.post(
            path=f"/rate/{derp.uuid}",
            follow=True,
            data=form_data,
        )
        assertContains(res, derp2.name)
        res = client.post(
            path=f"/rate/{derp2.uuid}",
            follow=True,
            data=form_data,
        )
        assertContains(res, "No Derps Left!")

    def test_anon_can_rate(self, derp, client):
        derp2 = DerpFactory()
        derp2.save()
        form_data = {
            "value": "10",
        }
        res = client.post(
            path=f"/rate/{derp.uuid}",
            follow=True,
            data=form_data,
        )
        assertContains(res, derp2.name)
        res = client.post(
            path=f"/rate/{derp2.uuid}",
            follow=True,
            data=form_data,
        )
        assertContains(res, "No Derps Left!")

    def test_increments_user_derps_rated(self, derp, client, rf):
        profile = UserProfileFactory()
        form_data = {
            "value": "10",
        }
        assert profile.n_derps_rated == 0
        req = rf.post(
            path=f"/rate/{derp.uuid}",
            data=form_data,
        )
        req.user = profile.user
        res = rate_derp_view(req, derp_uuid=derp.uuid)
        assert profile.n_derps_rated == 1
