from factory.django import DjangoModelFactory
import factory, factory.fuzzy
from pytest import fixture

from ..models import DerpRating
from ratemyderp.users.tests.factories import DerpUserFactory
from ratemyderp.derps.tests.factories import DerpFactory


@fixture
def rating():
    return DerpRatingFactory()


@fixture
def rating_session():
    return SessionDerpRatingFactory()


@fixture
def rating_user():
    return UserDerpRatingFactory()


@fixture
def ratings_batch_for_derp(derp):
    return UserDerpRatingFactory().create_batch(size=100, derp=derp)


class DerpRatingFactory(DjangoModelFactory):
    derp = factory.SubFactory(DerpFactory)

    class Meta:
        model = DerpRating


class UserDerpRatingFactory(DjangoModelFactory):
    user = factory.SubFactory(DerpUserFactory)
    derp = factory.SubFactory(DerpFactory)
    value = factory.fuzzy.FuzzyChoice([x + 1 for x in range(0, 10)])

    class Meta:
        model = DerpRating


class SessionDerpRatingFactory(DjangoModelFactory):
    session_id = factory.Faker("uuid4")
    is_anonymous = True
    derp = factory.SubFactory(DerpFactory)
    value = factory.fuzzy.FuzzyChoice([x + 1 for x in range(0, 10)])

    class Meta:
        model = DerpRating
