from django.contrib.auth import get_user_model
from django.contrib.sessions.backends.db import SessionStore
import uuid


User = get_user_model()

# -------------------- Get or Create Anonymous Session ID -------------------- #
def get_or_create_anon_id(request):
    """
    Generate a new session-based ID for anonymous users, or
    retrieve their current session ID if it exists.
    - Called when there is no authenticated user present
    """
    # if the key doesn't already exist, we just have to save it in the db first
    if not request.session.session_key:
        request.session["next_derp_id"] = ""
        request.session.save()
        print(request.session)
    return request.session.session_key


class UserOrAnon:
    """
    Used throughout the app to lazily distinguish between anonymous cookie
    users and registered users.
    """

    def __init__(self, user_or_anon_id):
        """`user_or_anon_id`: pass a User object or an anonymous session_key uuid"""
        self.obj = user_or_anon_id
        self.is_anon = False if isinstance(self.obj, User) else True

    @classmethod
    def from_request(cls, request):
        """generate a new UserOrAnon from a request object"""
        # print(get_or_create_anon_id(request))
        # if hasattr(request, "user")
        return cls(
            request.user
            if request.user.is_authenticated
            else get_or_create_anon_id(request),
        )

    @classmethod
    def assert_param_type(cls, param):
        assert isinstance(
            param, cls
        ), f"Error: parameter {param} must be instance of type UserOrAnon"

    def get_next_derp_id_to_rate(self):
        """
        Gets existing (cached) derp UUID to rate, or None if
        it's not set.
        Handles both anon or auth-based users.
        Prevents refreshing the derp rating page to "cheat" by not voting
        to get a new Derp to see.
        """
        derp_id_str = (
            SessionStore(session_key=self.obj)["next_derp_id"]
            if self.is_anon
            else self.obj.profile.next_derp_id
        )
        return None if derp_id_str == "" else uuid.UUID(derp_id_str)

    def set_next_derp_id_to_rate(self, derp_uuid):
        """
        Sets the next derp to be rated by this user/session (by UUID)
        The system won't give the user a new one until they have voted
        on this one.
        `derp_uuid`: UUID of the next derp to rate
        """
        if self.is_anon:
            session = SessionStore(session_key=self.obj)
            session["next_derp_id"] = str(derp_uuid)
            session.save()
            pass

        else:
            self.obj.profile.next_derp_id = str(derp_uuid)
            self.obj.profile.save()

    def set_next_derp_to_rate(self, derp):
        return self.set_next_derp_id_to_rate(
            derp.uuid if derp is not None else ""
        )
