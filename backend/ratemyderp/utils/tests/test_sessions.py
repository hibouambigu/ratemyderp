import pytest
from django.contrib.sessions.backends.db import SessionStore
from django.contrib import auth

from ratemyderp.utils.sessions import get_or_create_anon_id, UserOrAnon

pytestmark = pytest.mark.django_db


class TestUserOrAnon:
    def test_init_with_user(self, user):
        u = UserOrAnon(user)
        assert u.obj == user
        assert u.is_anon == False

    def test_init_with_anon(self, client):
        anon_id = client.session.session_key
        u = UserOrAnon(anon_id)
        assert u.obj == anon_id
        assert u.is_anon == True

    def test_from_request_anon(self, client):
        anon_id = client.session.session_key
        # makes user.is_authenticated work:
        setattr(client, "user", auth.get_user(client))
        u = UserOrAnon.from_request(client)
        assert u.obj == anon_id

    def test_from_request_user(self, rf, user):
        req = rf.get("/")
        req.user = user
        u = UserOrAnon.from_request(req)
        assert u.obj == user


class TestGetOrCreateAnonId:
    def test_creates_new(self, client):
        assert get_or_create_anon_id(client) != None

    def test_gets_existing(self, client):
        anon_id = client.session.session_key
        assert get_or_create_anon_id(client) == anon_id


class TestNextDerpToRate:
    def test_get_next_derp_id_with_user(self, user, derp):
        u = UserOrAnon(user)
        user.profile.next_derp_id = str(derp.uuid)
        user.profile.save()
        assert u.obj.profile.next_derp_id is not None
        assert u.get_next_derp_id_to_rate() == derp.uuid

    def test_get_next_derp_id_with_user_returns_none(self, user):
        u = UserOrAnon(user)
        user.profile.next_derp_id = ""
        user.profile.save()
        assert u.get_next_derp_id_to_rate() is None

    def test_get_next_derp_id_with_anon(self, client, derp):
        req = client
        session = req.session
        session["next_derp_id"] = str(derp.uuid)
        session.save()
        # makes user.is_authenticated work:
        setattr(req, "user", auth.get_user(req))
        u = UserOrAnon.from_request(req)
        assert u.get_next_derp_id_to_rate() == derp.uuid

    def get_next_derp_id_with_anon_returns_none(self, client):
        req = client
        session = req.session
        session.save()
        u = UserOrAnon.from_request(req)
        assert u.get_next_derp_id_to_rate() is None

    def test_set_next_derp_id_with_user(self, user, derp):
        u = UserOrAnon(user)
        u.set_next_derp_id_to_rate(derp.uuid)
        assert u.obj.profile.next_derp_id == str(derp.uuid)

    def test_set_next_derp_id_with_anon(self, client, derp):
        req = client
        # makes user.is_authenticated work:
        setattr(client, "user", auth.get_user(client))
        u = UserOrAnon.from_request(req)
        u.set_next_derp_id_to_rate(derp.uuid)
        s = SessionStore(session_key=u.obj)
        assert s["next_derp_id"] == str(derp.uuid)
