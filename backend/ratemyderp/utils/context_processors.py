from django.contrib.sites.models import Site


def base_url_context_processor(request):
    """
    Gets the base url of the application in a template.
    """
    return {"BASE_URL": "https://%s" % Site.objects.get_current().domain}
    # or if you don't want to use 'sites' app
    return {"BASE_URL": request.build_absolute_uri("/").rstrip("/")}
