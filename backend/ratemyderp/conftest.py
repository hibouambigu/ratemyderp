import pytest
from django.test import RequestFactory

from ratemyderp.users.models import DerpUser
from ratemyderp.users.tests.factories import DerpUserFactory
from ratemyderp.derps.models import Derp
from ratemyderp.derps.tests.factories import DerpFactory
from ratemyderp.rating.models import DerpRating
from ratemyderp.rating.tests.factories import (
    SessionDerpRatingFactory,
    UserDerpRatingFactory,
)


@pytest.fixture(autouse=True)
def media_storage(settings, tmpdir):
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture
def user() -> DerpUser:
    return DerpUserFactory()


@pytest.fixture
def derp() -> Derp:
    return DerpFactory()


@pytest.fixture
def rated_derp():
    from ratemyderp.rating.tests.factories import SessionDerpRatingFactory

    derp = DerpFactory()
    ratings = SessionDerpRatingFactory.create_batch(100, derp=derp)
    for rating in ratings:
        rating.apply()
    anon_ratings = SessionDerpRatingFactory.create_batch(100, derp=derp)
    for rating in anon_ratings:
        rating.apply()
    return derp


@pytest.fixture
def anon_rating() -> DerpRating:
    return SessionDerpRatingFactory()


@pytest.fixture
def user_rating() -> DerpRating:
    return UserDerpRatingFactory()
