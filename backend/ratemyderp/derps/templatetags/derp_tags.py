from django import template
from ratemyderp.derps.models import Derp

register = template.Library()

@register.filter(name='derp_species_icon')
def derp_species_icon(value):
  """Get derp species icon to render in template"""
  return Derp.species_to_icon_str(value)