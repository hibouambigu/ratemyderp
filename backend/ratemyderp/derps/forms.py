from django.forms import (
  ModelForm, 
  RadioSelect, 
  Textarea,
  FileInput
)
from ratemyderp.derps.models import Derp

class DerpForm(ModelForm):
  class Meta:
    model = Derp
    fields = [
      'name', 'image', 'species', 'age_when_added', 'bio'
    ]
    widgets = {
      'species': RadioSelect,
      'bio': Textarea(attrs={'rows': 4}),
      'image': FileInput(attrs={'class': 'image-upload'})
    }

    # TODO: validate name does not already exist; must be unique
    # eg https://www.agiliq.com/blog/2019/01/django-createview/