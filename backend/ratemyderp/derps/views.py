from django.views.generic import DetailView, CreateView, DeleteView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import Http404

from ratemyderp.derps.models import Derp
from ratemyderp.derps.forms import DerpForm
from ratemyderp.rating.models import DerpRating
from ratemyderp.utils.sessions import UserOrAnon


class DerpCreateView(LoginRequiredMixin, CreateView):
    form_class = DerpForm
    model = Derp

    # inject owner fkey after form validation occurs
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


derp_create_view = DerpCreateView.as_view()


class DerpDetailView(DetailView):
    model = Derp

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # add existing rating from this user to template context, if it exists
        if self.request.user is not None:
            existing_rating = DerpRating.get_existing_or_none(
                self.request.user, self.get_object()
            )
            ctx["user_rating"] = (
                existing_rating.value if existing_rating is not None else None
            )
        return ctx

    def get(self, request, *args, **kwargs):
        res = super().get(request, *args, **kwargs)
        # increment visit counter for this Derp
        user = UserOrAnon.from_request(request)
        self.object.increment_visit_count(user)
        return res


derp_detail_view = DerpDetailView.as_view()


class DerpShareView(DetailView):
    model = Derp
    template_name = "derps/derp_share.html"

    def get(self, request, *args, **kwargs):
        res = super().get(request, *args, **kwargs)
        # increment the share counter for this Derp
        self.get_object().increment_share_count()
        return res


derp_share_view = DerpShareView.as_view()


class Top10DerpsView(ListView):
    model = Derp
    template_name = "derps/derps_top10.html"
    queryset = Derp.objects.all().order_by("-rating_val")[:10]
    context_object_name = "derps"


top10_derps_view = Top10DerpsView.as_view()


class DerpDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Derp
    template_name = "derps/derp_delete.html"
    success_message = ""

    def get_object(self, queryset=None):
        derp = super(DerpDeleteView, self).get_object()
        self.user = self.request.user
        if derp.owner != self.user:
            raise Http404
        return derp

    def get_success_url(self):
        return self.user.get_absolute_url()

    def get_success_message(self, cleaned_data):
        return f"Deleted {self.object.name} successfully."


derp_delete_view = DerpDeleteView.as_view()
