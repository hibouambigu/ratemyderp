from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


def validate_age(value):
    """
    Sane numerical age input validation.
    """
    if not 0.25 <= value <= 99:
        msg = "Age must be 0.25 to 99 years"
        raise ValidationError(msg)

    if not value % 0.25 == 0:
        msg = "Must be a multiple of 0.25"
        raise ValidationError(msg)


def validate_derp_name(value):
    """
    Sane derp name charfield validation
    RegEx: [a-zA-Z]*([a-zA-Z]+\s?){1,3}[a-zA-Z]+
    """
    nums = sum(c.isdigit() for c in value)
    chars = sum(c.isalpha() for c in value)
    spaces = sum(c.isspace() for c in value)
    others = len(value) - nums - chars - spaces

    if spaces > 3:
        msg = "Name cannot have more than 3 spaces"
        raise ValidationError(msg)

    if nums > 0:
        msg = "Digits not allowed. Try Roman numerals instead!"
        raise ValidationError(msg)

    if others > 0:
        msg = "Alphabetic characters only"
        raise ValidationError(msg)

    if not value.count("  ") < 1:
        msg = "No double spaces"
        raise ValidationError(msg)

    if not value.count("   ") < 1:
        msg = "No triple spaces"
        raise ValidationError(msg)

    if value[0].isspace():
        msg = "No leading spaces"
        raise ValidationError(msg)

    if len(value) < 3:
        msg = "Minimum length is 3 characters"
        raise ValidationError(msg)
