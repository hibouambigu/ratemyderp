import pytest
from dateutil.relativedelta import relativedelta

from ..models import Derp
from ratemyderp.utils.sessions import UserOrAnon
from ratemyderp.derps.tests.factories import DerpFactory

pytestmark = pytest.mark.django_db


class TestDerpModelIconStr:
    def test_species_icon_str_bad_input(self):
        # no bad input
        assert Derp.species_to_icon_str("DERP") == "fa-solid fa-circle-question"


class TestDerpModelBase:
    def test__str__(self, derp):
        assert derp.__str__() == derp.name
        assert str(derp) == derp.name

    def test_get_absolute_url(self, derp):
        url = derp.get_absolute_url()
        assert url == f"/derp/{derp.uuid}"

    def test_get_date_created_pretty(self, derp):
        assert derp.get_date_created_pretty() == derp.created.strftime(
            "%b %e %Y"
        )


class TestDerpModelRank:
    def test_set_rank_changes(self, derp):
        derp.set_rank(3)
        assert derp.rank == 3

    def test_set_rank_is_int(self, derp):
        derp.set_rank(3.5)
        assert derp.rank != 0 and derp.rank % 1 == 0

    def test_update_all_rankings(self):
        d1, d2, d3 = DerpFactory(), DerpFactory(), DerpFactory()
        d1.rating_val = 7.5
        d2.rating_val = 8.5
        d3.rating_val = 9.5
        d1.save()
        d2.save()
        d3.save()
        Derp.update_all_rankings()
        d1.refresh_from_db()
        d2.refresh_from_db()
        d3.refresh_from_db()
        assert d1.rank == 3
        assert d2.rank == 2
        assert d3.rank == 1


class TestDerpModelQueries:
    def test_get_by_uuid(self, derp):
        assert Derp.get_by_uuid(derp.uuid) == derp


class TestDerpModelApplyRating:
    def test_positive(self, derp):
        derp.apply_rating(10, 1)
        assert (
            derp.rating_val == 10
            and derp.rating_n_votes == 1
            and derp.rating_acc == 10
        )

    def test_negative(self, derp):
        derp.apply_rating(5, 1)
        derp.apply_rating(10, 1)
        derp.apply_rating(10, -1)
        assert (
            derp.rating_val == 5
            and derp.rating_n_votes == 1
            and derp.rating_acc == 5
        )

    def test_does_not_underrun(self, derp):
        derp.apply_rating(10, -5)
        assert (
            derp.rating_val == 0
            and derp.rating_n_votes == 0
            and derp.rating_acc == 0
        )

    def test_zero_weight(self, derp):
        derp.apply_rating(10, 0)
        assert (
            derp.rating_val == 0
            and derp.rating_n_votes == 0
            and derp.rating_acc == 0
        )

    def test_zero_vote(self, derp):
        derp.apply_rating(0, 0)
        assert (
            derp.rating_val == 0
            and derp.rating_n_votes == 0
            and derp.rating_acc == 0
        )

    def test_compound_vote(self, derp):
        N_VOTES = 1
        ACC = 10 + (9 * 3) - 8
        AVG = ACC / N_VOTES
        derp.apply_rating(10, 1)
        derp.apply_rating(9, 3)  # larger weight
        derp.apply_rating(8, -1)  # negative weight
        assert (
            derp.rating_val == AVG
            and derp.rating_n_votes == N_VOTES
            and derp.rating_acc == ACC
        )


class TestDerpModelAddRating:
    def test_zero(self, derp):
        derp.add_rating(0)
        assert derp.rating_val == 0

    def test_add(self, derp):
        derp.add_rating(10)
        assert (
            derp.rating_val == 10
            and derp.rating_n_votes == 1
            and derp.rating_acc == 10
        )


class TestDerpModelSubtractRating:
    def test_zero(self, derp):
        derp.subtract_rating(0)
        assert derp.rating_val == 0

    def test_compound(self, derp):
        derp.rating_val = 10
        derp.rating_acc = 10
        derp.rating_n_votes = 1
        derp.subtract_rating(10)
        derp.subtract_rating(5)
        assert (
            derp.rating_val == 0
            and derp.rating_n_votes == 0
            and derp.rating_acc == 0
        )


class TestDerpModelGetDerpsAlreadyRated:
    def test_returns_user_derps(self, user_rating):
        u = UserOrAnon(user_rating.user)
        derps = Derp.get_derps_already_rated(u)
        assert derps.first() == user_rating.derp

    def test_returns_anon_derps(self, anon_rating):
        u = UserOrAnon(anon_rating.session_id)
        derps = Derp.get_derps_already_rated(u)
        assert derps.first() == anon_rating.derp

    def test_returns_empty_queryset(self, user_rating, user):
        assert Derp.objects.all().first() == user_rating.derp
        u = UserOrAnon(user)
        derps = Derp.get_derps_already_rated(u)
        assert not derps.exists()


class TestDerpModelGetFreshDerp:
    def test_returns_fresh_user(self, user_rating, user):
        derp = user_rating.derp
        u = UserOrAnon(user)
        fresh_derp = Derp.get_fresh_or_none(u)
        assert fresh_derp == derp

    def test_returns_fresh_anon(self, anon_rating, client):
        derp = anon_rating.derp
        anon_id = client.session.session_key
        u = UserOrAnon(anon_id)
        fresh_derp = Derp.get_fresh_or_none(u)
        assert fresh_derp == derp

    def test_returns_none(self, user_rating):
        u = UserOrAnon(user_rating.user)
        fresh_derp = Derp.get_fresh_or_none(u)
        assert fresh_derp == None


class TestDerpModelIncrementVisitStat:
    def test_user_increases_count(self, user, derp):
        u = UserOrAnon(user)
        assert derp.n_visits == 0
        derp.increment_visit_count(u)
        d = Derp.objects.get(name=derp.name)
        assert d.n_visits == 1
        assert d.n_user_visits == 1
        assert d.n_anon_visits == 0

    def test_anon_increases_count(self, derp, client):
        u = UserOrAnon(client.session.session_key)
        assert derp.n_visits == 0
        derp.increment_visit_count(u)
        d = Derp.objects.get(name=derp.name)
        assert d.n_visits == 1
        assert d.n_user_visits == 0
        assert d.n_anon_visits == 1


class TestDerpModelIncrementShareStat:
    def test_count_increases(self, derp):
        assert derp.n_shares == 0
        derp.increment_share_count()
        d = Derp.objects.get(name=derp.name)
        assert d.n_shares == 1


class TestDerpModelAge:
    def test_age_in_years(self, derp):
        derp.age_when_added = 3.25
        assert derp.get_age() == 3.25

    def test_birthday(self, derp):
        derp.age_when_added = 1.5
        bday = derp.created + relativedelta(years=-1, months=-6)
        assert derp.get_birthday() == bday

    def test_age_display(self, derp):
        derp.age_when_added = 1.5
        assert derp.get_age_display() == f"1y 6mo old"

    def test_age_display_zero_years(self, derp):
        derp.age_when_added = 0.25
        assert derp.get_age_display() == f"3mo old"

    def test_age_display_zero_months(self, derp):
        derp.age_when_added = 2.0
        assert derp.get_age_display() == f"2y old"
