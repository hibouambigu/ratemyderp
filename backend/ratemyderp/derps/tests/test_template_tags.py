import pytest
from django.template import Context, Template
from pytest_django.asserts import assertContains
from ratemyderp.derps.models import Derp

pytestmark = pytest.mark.django_db


def test_derp_species_icon_filter():
    "derp species icon filter renders in template"
    SPECIES = Derp.Species.FELINE
    CTX = Context({"species": SPECIES})
    ICON_STR = Derp.species_to_icon_str(SPECIES)
    TEMPLATE = Template("{% load derp_tags %} {{ species|derp_species_icon }}")
    assert ICON_STR in TEMPLATE.render(CTX)
