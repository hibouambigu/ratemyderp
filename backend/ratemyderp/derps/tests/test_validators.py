import pytest
from django.core.exceptions import ValidationError

from ratemyderp.derps.validators import validate_age, validate_derp_name


class TestValidateAge:
    def test_good_input(self):
        try:
            validate_age(3.25)
        except Exception as ex:
            assert False, f"Exception was raised: {ex}"

    def test_too_low(self):
        with pytest.raises(ValidationError):
            validate_age(0.0)

    def test_too_high(self):
        with pytest.raises(ValidationError):
            validate_age(100)

    def test_not_multiple_of_quarter(self):
        with pytest.raises(ValidationError):
            validate_age(3.33)

    def test_good_but_strange_input(self):
        try:
            validate_age(1.500000)
        except Exception as ex:
            assert False, f"Exception was raised: {ex}"


class TestValidateDerpName:
    def test_good_input(self):
        try:
            validate_derp_name("Queen Guppy Huntingdon VI")
        except Exception as ex:
            assert False, f"Exception was raised: {ex}"

    def test_no_more_than_3_spaces_total(self):
        with pytest.raises(ValidationError):
            validate_derp_name("beans beans beans beans beans")

    def test_no_double_spaces(self):
        with pytest.raises(ValidationError):
            validate_derp_name("beans  beans")

    def test_no_triple_space(self):
        with pytest.raises(ValidationError):
            validate_derp_name("beans   beans")

    def test_no_symbols(self):
        with pytest.raises(ValidationError):
            validate_derp_name("beans--beans")

    def test_no_numbers(self):
        with pytest.raises(ValidationError):
            validate_derp_name("beans 123")

    def test_no_leading_space(self):
        with pytest.raises(ValidationError):
            validate_derp_name(" beans")

    def test_length_min_3_chars(self):
        with pytest.raises(ValidationError):
            validate_derp_name("be")
