import pytest
import factory, factory.fuzzy, factory.faker
from factory.django import DjangoModelFactory

from ..models import Derp
from ratemyderp.users.tests.factories import DerpUserFactory


class DerpFactory(DjangoModelFactory):
    class Meta:
        model = Derp

    name = factory.Faker("name")
    owner = factory.SubFactory(DerpUserFactory)
    species = factory.fuzzy.FuzzyChoice([x[0] for x in Derp.Species.choices])
    age_when_added = factory.fuzzy.FuzzyChoice([x / 2 for x in range(1, 5)])
    bio = factory.Faker("paragraph", variable_nb_sentences=True)
    image = factory.django.ImageField(color="blue", width=1080, height=1080)


@pytest.fixture
def derp():
    return DerpFactory()


@pytest.fixture
def rated_derp():
    from ratemyderp.rating.tests.factories import SessionDerpRatingFactory

    derp = DerpFactory()
    ratings = SessionDerpRatingFactory.create_batch(100, derp=derp)
    for rating in ratings:
        rating.apply()
    return derp
