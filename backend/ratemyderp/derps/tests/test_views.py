import pytest
from pytest_django.asserts import assertContains
from django.urls import reverse
from django.core.files.uploadedfile import SimpleUploadedFile

from ratemyderp.derps.views import derp_detail_view, derp_create_view
from ratemyderp.users.tests.factories import DerpUserFactory
from ratemyderp.rating.tests.factories import SessionDerpRatingFactory
from ratemyderp.derps.models import Derp
from ratemyderp.derps.tests.factories import DerpFactory


pytestmark = pytest.mark.django_db


class TestDerpDetailView:
    def test_response_is_ok(self, rf, derp, user):
        req = rf.get(reverse("derp:detail", kwargs={"pk": derp.uuid}))
        req.user = user
        res = derp_detail_view(req, pk=derp.uuid)
        # assertContains *also* verifies HTTP 200 status..
        assertContains(res, derp.name)

    def test_visit_count_is_incremented(self, rf, derp, user):
        req = rf.get(reverse("derp:detail", kwargs={"pk": derp.uuid}))
        req.user = user
        res = derp_detail_view(req, pk=derp.uuid)
        assertContains(res, derp.name)
        d = Derp.objects.get(name=derp.name)
        assert d.n_visits == 1
        assert d.n_user_visits == 1

    def test_contains_derp_data(self, rf, rated_derp, user):
        derp = rated_derp
        req = rf.get(reverse("derp:detail", kwargs={"pk": derp.uuid}))
        req.user = user
        res = derp_detail_view(req, pk=derp.uuid)
        assertContains(res, derp.name)
        assertContains(res, derp.image)
        assertContains(res, derp.owner.username)
        assertContains(res, derp.rating_n_votes)
        assertContains(res, derp.rank)
        assertContains(res, derp.get_date_created_pretty())


class TestDerpCreateView:
    def test_response_is_ok(self, user, client):
        client.force_login(user)
        res = client.get(reverse("derp:submit"))
        assert res.status_code == 200

    def test_correct_title(self, user, client):
        client.force_login(user)
        res = client.get(reverse("derp:submit"))
        assertContains(res, "Submit Derp")

    def test_creation_success(self):
        # TODO
        pass

        # u = DerpUserFactory()
        # client.force_login(u)
        # u.save()
        # img = SimpleUploadedFile("image.jpg", b"file_content")
        # form_data = {
        #     "name": "Testy Tom",
        #     "image": img,
        #     "species": Derp.Species.FELINE,
        #     "age_when_added": 1.5,
        #     "bio": "Dolore irure in sint cillum nostrud eiusmod officia tempor eiusmod.",
        #     "owner": user,
        # }
        # DerpUser.objects.get(username=u.name)

        # # res = client.post(reverse("derp:submit"), form_data)
        # # redirect to newly created derp is ok
        # # assert res.status_code == 302
        # # validate created derp data
        # # created = Derp.objects.get(name="Stats 2")
