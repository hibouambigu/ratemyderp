from django.contrib import admin
from ratemyderp.derps.models import Derp


@admin.register(Derp)
class DerpAdmin(admin.ModelAdmin):
    # columns to display
    list_display = (
        "name",
        "created",
        "rating_val",
        "rank",
        "rating_n_votes",
        "n_visits",
        "n_shares",
        "owner",
        "species",
        "uuid",
    )
    # filter by owner and species category
    list_filter = ("owner", "species")
    # search by name, owner and species
    search_fields = ("name", "owner", "species")
    # 25 per page displayed
    list_per_page = 25
