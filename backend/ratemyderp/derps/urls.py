from django.urls import path
from ratemyderp.derps.views import (
    derp_detail_view,
    derp_create_view,
    derp_delete_view,
    derp_share_view,
)

app_name = "derp"
urlpatterns = [
    path(route="<uuid:pk>", view=derp_detail_view, name="detail"),
    path(route="submit", view=derp_create_view, name="submit"),
    path(route="<uuid:pk>/delete", view=derp_delete_view, name="delete"),
    path(route="<uuid:pk>/share", view=derp_share_view, name="share"),
]
