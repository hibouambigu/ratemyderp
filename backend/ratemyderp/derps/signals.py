# # signals
# from django.dispatch import receiver
# from django.db.models.signals import post_save

# from ratemyderp.derps.models import Derp
# from ratemyderp.rating.models import DerpRating


# # -------------------------- Derp Creation Post-Save ------------------------- #
# @receiver(post_save, sender=Derp)
# def instantiate_derp_stats(sender, instance, created, **kwargs):
#     if created:
#         instance.rating = DerpRating()
#         instance.rating.save()
#         instance.save()
