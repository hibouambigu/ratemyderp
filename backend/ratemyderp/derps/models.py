from django.db import models
from django.conf import settings
from django.urls import reverse
from django.shortcuts import get_object_or_404
from random import choice
from model_utils.models import TimeStampedModel
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from dateutil.relativedelta import relativedelta
from datetime import datetime, timezone
from math import floor

from ratemyderp.utils.sessions import UserOrAnon
from ratemyderp.derps.validators import validate_age, validate_derp_name

import uuid


# -------------------------------- Derp Model -------------------------------- #
class Derp(TimeStampedModel):
    """
    Models each Derp entry in the database.
    """

    # derp PK is UUID4
    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    # user who submitted this derp
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE
    )
    # the derp's name
    name = models.CharField(max_length=50, validators=[validate_derp_name])
    # species choices
    class Species(models.TextChoices):
        CANINE = "DOG", "Canine"
        FELINE = "CAT", "Feline"
        REPTILE = "REP", "Reptile"
        FISH = "FSH", "Fishy"
        SMALL = "SML", "Lil' Thing"
        BIRB = "BRB", "Birb"
        OTHER = "OTH", "Other"

    # derp species
    species = models.CharField(
        "Species", max_length=3, choices=Species.choices, default=Species.OTHER
    )

    # age in years
    age_when_added = models.FloatField(validators=[validate_age])
    # biography text
    bio = models.TextField(max_length=200, blank=True)
    # derp image
    # image = models.ImageField(upload_to="images/%Y/%m/%d/")
    image = ProcessedImageField(
        upload_to="derps/%Y/%m/%d/",
        processors=[ResizeToFill(1080, 1080)],
        format="JPEG",
        options={"quality": 70},
    )

    # derp statistics
    rank = models.IntegerField(default=0, editable=False, blank=True)
    n_shares = models.IntegerField(default=0, editable=False, blank=True)
    n_awards = models.IntegerField(default=0, editable=False, blank=True)
    n_flags = models.IntegerField(default=0, editable=False, blank=True)
    # visits
    n_visits = models.IntegerField(default=0, editable=False, blank=True)
    n_user_visits = models.IntegerField(default=0, editable=False, blank=True)
    n_anon_visits = models.IntegerField(default=0, editable=False, blank=True)
    # rating fields
    rating_n_votes = models.IntegerField(default=0, editable=False, blank=True)
    """total number of rating votes cast"""
    rating_acc = models.IntegerField(default=0, editable=False, blank=True)
    """accumulator for rating calculations"""
    rating_val = models.FloatField(default=0, blank=True, editable=False)
    """the cumulative average rating value at this point in time"""

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("derp:detail", kwargs={"pk": self.uuid})

    def species_icon(self):
        """
        String rep'n of Derp's species icon
        """
        return Derp.species_to_icon_str(self.species)

    def get_date_created_pretty(self):
        return self.created.strftime("%b %e %Y")

    # --------------------------------- Derp Age --------------------------------- #
    def get_age(self):
        """
        Return the current age in years of the Derp based on its age when created.
        """
        return round(
            ((datetime.now(timezone.utc) - self.get_birthday()).days / 365),
            2,
        )

    def get_birthday(self):
        """
        Returns a datetime instance representing the birthday.
        Approximated based on the user-submitted age during creation.
        """
        return self.created + relativedelta(months=-self.age_when_added * 12)

    def get_age_display(self):
        """
        Formats the age of the Derp into a display string of the form
        "00y 00mo old" for use in templates.
        """
        age_years = self.get_age()
        y = floor(age_years)
        m = (age_years - y) * 12
        y_old = f"{int(y)}y " if y != 0 else ""
        m_old = f"{int(m)}mo " if m != 0 else ""
        return y_old + m_old + "old"

    # ---------------------------- Add/Subtract Rating --------------------------- #
    def apply_rating(self, rating, weight):
        """
        Apply a rating to this Derp, generically.
        Sign of weight implies add/subtract influence to rating.
        """
        # 1. add rating to accumulator and increment count
        n_votes = self.rating_n_votes + (1 if weight > 0 else -1)
        n_votes = 0 if n_votes < 0 else n_votes
        rating_acc = self.rating_acc + int(rating) * weight
        if rating_acc < 0:
            rating_acc = 0
        # 2. compute new rating
        new_rating = 0 if n_votes == 0 else (float(rating_acc) / n_votes)
        # 3. update & save the Derp
        self.rating_n_votes = n_votes
        self.rating_acc = rating_acc
        self.rating_val = new_rating
        self.save()
        return new_rating

    def add_rating(self, rating, weight=1):
        """
        Adds a vote's influence to the Rating calculation.
        """
        return self.apply_rating(rating, weight=abs(weight))

    def subtract_rating(self, rating, weight=1):
        """
        Subtracts a vote's influence from the Rating calculation.
        """
        return self.apply_rating(rating, weight=-abs(weight))

    # --------------------------- Increment Visit Count -------------------------- #
    def increment_visit_count(self, user_or_anon):
        """
        Increments this Derp's visit count stat.
        Categorises visits based on whether they are registered or anon users.
        """
        UserOrAnon.assert_param_type(user_or_anon)
        self.n_visits += 1
        if user_or_anon.is_anon:
            self.n_anon_visits += 1
        else:
            self.n_user_visits += 1
        self.save()

    # --------------------------- Increment Share Count -------------------------- #
    def increment_share_count(self):
        """
        Increments the share counter for this Derp.
        """
        self.n_shares += 1
        self.save()

    # --------------------------------- Set Rank --------------------------------- #
    def set_rank(self, new_rank):
        """
        Updates the positional rank stat for this Derp.
        Only writes Derp object back to db if rank has changed.
        """
        rank = int(new_rank)
        if rank != self.rank:
            self.rank = rank
            self.save()

    @classmethod
    def get_by_uuid(cls, uuid):
        """
        Query a given Derp by uuid.
        ### DO NOT USE OUTSIDE OF VIEWS!
        """
        return get_object_or_404(cls, uuid=uuid)

    @classmethod
    def species_to_icon_str(cls, species):
        """Returns FontAwesome icon string to use from a Derp.Species value"""
        Opt = cls.Species
        match species:
            case Opt.CANINE:
                return "fa-solid fa-dog"
            case Opt.FELINE:
                return "fa-solid fa-cat"
            case Opt.REPTILE:
                return "fa-solid fa-dragon"
            case Opt.FISH:
                return "fa-solid fa-fish"
            case Opt.SMALL:
                return "fa-solid fa-bug"
            case Opt.BIRB:
                return "fa-solid fa-kiwi-bird"
            case Opt.OTHER:
                return "fa-solid fa-circle-question"
            case _:
                return "fa-solid fa-circle-question"

    @classmethod
    def get_derps_already_rated(cls, user_or_anon):
        """
        Gets all the Derps a user has already rated.
        Returns a queryset.
        """
        UserOrAnon.assert_param_type(user_or_anon)
        return (
            cls.objects.filter(derprating__session_id=user_or_anon.obj)
            if user_or_anon.is_anon
            else cls.objects.filter(derprating__user=user_or_anon.obj)
        )

    @classmethod
    def get_fresh_or_none(cls, user_or_anon):
        """
        Retrieves a random "fresh" Derp from the database.
        Returns a Derp object or None.
        If there are no derps which have not been voted on by the request.user left,
        we return None instead of a Derp object.
        """
        UserOrAnon.assert_param_type(user_or_anon)

        # 1. query all the derps this user has rated
        derps_already_rated = cls.get_derps_already_rated(user_or_anon)
        # 2. query derps excluding ones in derps_voted_on
        fresh_derps = cls.objects.all().difference(derps_already_rated)
        # 3. we return none if the user has rated every derp in the db;
        #   else a random fresh one
        fresh_derp = choice(fresh_derps) if fresh_derps.exists() else None
        user_or_anon.set_next_derp_to_rate(fresh_derp)
        return fresh_derp

    @classmethod
    def update_all_rankings(cls):
        """
        Update the positional ranking stat of all Derps in the DB.
        Should be called periodically to re-compute derp rankings.
        """
        derps = cls.objects.all().order_by("-rating_val")
        for i, derp in enumerate(derps):
            derp.set_rank(i + 1)
