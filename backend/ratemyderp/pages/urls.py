from django.urls import path
from ratemyderp.pages.views import noderps_page_view, about_page_view
from ratemyderp.derps.views import top10_derps_view
from ratemyderp.rating.views import go_rating_view

urlpatterns = [
    path("noderps", noderps_page_view, name="noderps"),
    path("top10", top10_derps_view, name="top10"),
    path("", go_rating_view, name="go-rate"),
    path("about", about_page_view, name="about"),
]
