from django.views.generic import TemplateView


class HomePageView(TemplateView):
    template_name = "pages/home.html"


home_page_view = HomePageView.as_view()


class NoDerpsPageView(TemplateView):
    template_name = "pages/noderps.html"


noderps_page_view = NoDerpsPageView.as_view()


class AboutPageView(TemplateView):
    template_name = "pages/about.html"


about_page_view = AboutPageView.as_view()
