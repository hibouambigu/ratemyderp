from django.apps import AppConfig


class StatsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ratemyderp.stats"

    def ready(self):
        import ratemyderp.stats.periodic_tasks
