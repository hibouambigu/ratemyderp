import pytest
from celery.result import EagerResult

from ratemyderp.stats.tasks import get_rating_count
from ratemyderp.rating.tests.factories import DerpRatingFactory

pytestmark = pytest.mark.django_db


def test_rating_count(settings):
    """test DerpRating counting task"""
    DerpRatingFactory.create_batch(3)
    settings.CELERY_TASK_ALWAYS_EAGER = True
    task = get_rating_count.delay()
    assert isinstance(task, EagerResult)
    assert task.result == 3
