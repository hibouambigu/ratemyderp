# F'ns to send measurements to InfluxDB

from influxdb_client import Point
from django.db.models import Sum

from ratemyderp.derps.models import Derp
from ratemyderp.rating.models import DerpRating


def measure_all_metrics():
    """
    Measures all RateMyDerp metrics.
    Returns all metrics as an aggregated list of InfluxDB data points,
    ready for batch writing.
    """
    records = []
    records += measure_derps_n_derps()
    records += measure_n_ratings()
    records += measure_derps_n_visits()
    return records


def measure_derps_n_derps():
    """
    Measures number of derps by species.
    Returns measurements as a list of InfluxDB data points.
    """
    records = []
    for species in Derp.Species:
        # make a data point for each species
        count = Derp.objects.filter(species=species).count()
        point = (
            Point("derps")
            .tag("species", species.name.lower())
            .field("count", count)
        )
        records.append(point)
    return records


def measure_n_ratings():
    """
    Measures number of DerpRatings by user type.
    Returns InfluxDB data points as a list.
    """
    p_user_n_ratings = (
        Point("ratings")
        .tag("user_type", "authenticated")
        .field("count", DerpRating.objects.filter(is_anonymous=False).count())
    )
    p_anon_n_ratings = (
        Point("ratings")
        .tag("user_type", "anonymous")
        .field("count", DerpRating.objects.filter(is_anonymous=True).count())
    )
    return [p_user_n_ratings, p_anon_n_ratings]


def measure_derps_n_visits():
    """
    Measures the number of derp visits, sorted by species.
    Returns measurements as a list of InfluxDB data points.
    """
    records = []
    for species in Derp.Species:
        # make a data point for each species
        n_visits = Derp.objects.filter(species=species).aggregate(
            Sum("n_visits")
        )["n_visits__sum"]
        point = (
            Point("derps")
            .tag("species", species.name.lower())
            .field("visits", n_visits)
        )
        records.append(point)
    return records
