from config.celery_app import app

from ratemyderp.rating.models import DerpRating
from ratemyderp.derps.models import Derp
from ratemyderp.stats.measurements import measure_all_metrics
from ratemyderp.stats.utils import influx_batch_write, bucket


@app.task()
def get_rating_count():
    return DerpRating.objects.count()


@app.task()
def update_derp_rankings():
    """Update the positional ranking of all Derps in the db"""
    print("Computing all derp rankings!")
    Derp.update_all_rankings()


@app.task()
def write_timeseries_measurements():
    """
    Measures and batch writes all timeseries measurements to
    the configured InfluxDB server.
    """
    influx_batch_write(measure_all_metrics(), bucket)
