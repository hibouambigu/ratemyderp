from django.conf import settings
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.exceptions import InfluxDBError

"conf"
url = settings.INFLUX_URL
token = settings.INFLUX_TOKEN
bucket = settings.INFLUX_BUCKET
org = settings.INFLUX_ORG

"data"
points = [
    Point("derps").tag("species", "feline").field("count", 20),
    Point("derps").tag("species", "canine").field("count", 11),
    Point("derps").tag("species", "reptile").field("count", 5),
]


class BatchingCallback(object):
    def success(self, conf: str, data: str):
        """Successfully written batch."""
        print(f"Written batch: {conf}, data: {data}")

    def error(self, conf: str, data: str, exception: InfluxDBError):
        """Unsuccessfully written batch."""
        print(f"Cannot write batch: {conf}, data: {data} due: {exception}")

    def retry(self, conf: str, data: str, exception: InfluxDBError):
        """Retryable error."""
        print(
            f"Retryable error occurs for batch: {conf}, data: {data} retry: {exception}"
        )


def influx_batch_write(records, bucket):
    callback = BatchingCallback()
    with InfluxDBClient(url=url, token=token, org=org) as client:
        """
        InfluxDB batching API
        """
        with client.write_api(
            success_callback=callback.success,
            error_callback=callback.error,
            retry_callback=callback.retry,
        ) as write_api:
            write_api.write(bucket=bucket, record=records)
