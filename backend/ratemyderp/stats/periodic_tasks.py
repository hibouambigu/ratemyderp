from django.conf import settings

from config.celery_app import app
from .tasks import update_derp_rankings, write_timeseries_measurements


@app.on_after_finalize.connect
def setup_stats_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        settings.TASK_UPDATE_RANK_INTERVAL,
        update_derp_rankings.s(),
        name="update_derp_rankings",
    )
    sender.add_periodic_task(
        settings.TASK_TIMESERIES_MEASUREMENT_INTERVAL,
        write_timeseries_measurements.s(),
        name="write_timeseries_measurements_to_influxdb",
    )
