import pytest

from .factories import DerpUserFactory

pytestmark = pytest.mark.django_db


def test_user_creation_instantiates_profile():
    user = DerpUserFactory()
    user.save()
    assert user.profile is not None
