import pytest

from ratemyderp.users.models import DerpUser

pytestmark = pytest.mark.django_db


def test_user_get_abs_url(user: DerpUser):
    assert user.get_absolute_url() == f"/profile/user/{user.username}"
