from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.dispatch import receiver
from django.db.models.signals import post_save


class DerpUser(AbstractUser):
    """
    Custom user model for RateMyDerp!
    When adding fields which need to be filled out at signup, don't forget
    to modify forms.SignupForm and forms.SocialSignupForm accordingly
    """

    # accommodate patterns other than first + last name
    name = models.CharField("Name", blank=True, max_length=255)

    def get_absolute_url(self):
        return reverse("profile:detail", kwargs={"username": self.username})


# ----------------------- User -> Create Profile Signal ---------------------- #
@receiver(post_save, sender=DerpUser)
def instantiate_user_profile(sender, instance, created, **kwargs):
    from ratemyderp.profiles.models import DerpUserProfile

    if created:
        profile = DerpUserProfile(user=instance)
        profile.save()
