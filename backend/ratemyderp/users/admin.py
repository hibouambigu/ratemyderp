from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from ratemyderp.profiles.models import DerpUserProfile
from ratemyderp.users.forms import DerpUserCreationForm, DerpUserChangeForm

DerpUser = get_user_model()


class ProfileInline(admin.StackedInline):
    model = DerpUserProfile
    show_change_link = True


@admin.register(DerpUser)
class DerpUserAdmin(UserAdmin):
    form = DerpUserChangeForm
    add_form = DerpUserCreationForm
    model = DerpUser
    list_display = ["email", "username", "profile", "is_superuser"]
    search_fields = ["name"]
    inlines = [ProfileInline]
    list_select_related = True
