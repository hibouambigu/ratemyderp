# ---------------------------------------------------------------------------- #
#                                Users App Forms                               #
# ---------------------------------------------------------------------------- #

from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from allauth.account.forms import SignupForm
from allauth.socialaccount.forms import SignupForm as SocialSignupForm
from django.contrib.auth import forms as admin_forms


class DerpUserCreationForm(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = ('email', 'username',)

class DerpUserAdminChangeForm(admin_forms.UserChangeForm):
    """
    Admin change form for custom DerpUser
    """
    class Meta(admin_forms.UserChangeForm.Meta):
        model = get_user_model()


class DerpUserChangeForm(UserChangeForm):

    class Meta:
        model = get_user_model()
        fields = ('email', 'username',)


class UserAdminCreationForm(admin_forms.UserCreationForm):
    """
    Custom DerpUser admin area creation form
    """

    class Meta(admin_forms.UserCreationForm.Meta):
        model = get_user_model()
        error_messages = {
            "username": {"unique": "That username is already taken."}
        }

class DerpUserSignupForm(SignupForm):
  """
  Rendered by accounts signup view. Default fields automatically added.
  """


