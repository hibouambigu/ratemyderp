from django.shortcuts import get_object_or_404
from django.views.generic import ListView, UpdateView
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin

from ratemyderp.profiles.models import DerpUserProfile
from ratemyderp.profiles.forms import DerpUserProfileForm
from ratemyderp.derps.models import Derp

User = get_user_model()


class UserProfileView(ListView):
    model = Derp
    template_name = "profiles/profile_detail.html"
    context_object_name = "derps"
    paginate_by = 6

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.user = get_object_or_404(User, username=kwargs["username"])
        self.queryset = Derp.objects.filter(owner=self.user)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # we add the user's profile to the context (which already
        # contains their paginated derplist)
        ctx["profile"] = DerpUserProfile.objects.get(user=self.user)
        return ctx


user_profile_view = UserProfileView.as_view()


class UserProfileUpdateView(LoginRequiredMixin, UpdateView):

    model = DerpUserProfile
    template_name = "profiles/profile_update.html"
    form_class = DerpUserProfileForm
    context_object_name = "profile"

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.user = request.user

    def get_object(self):
        profile = get_object_or_404(DerpUserProfile, user=self.user)
        return profile


user_profile_update_view = UserProfileUpdateView.as_view()
