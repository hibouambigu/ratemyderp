from django.forms import ModelForm, Textarea, FileInput

from ratemyderp.profiles.models import DerpUserProfile


class DerpUserProfileForm(ModelForm):
    class Meta:
        model = DerpUserProfile
        fields = ["avatar", "is_private", "bio"]
        widgets = {
            "avatar": FileInput(attrs={"class": "image-upload"}),
            "bio": Textarea(attrs={"rows": 4}),
        }
