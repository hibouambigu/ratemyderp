import pytest

from .factories import UserProfileFactory, profile
from ratemyderp.profiles.models import DerpUserProfile

pytestmark = pytest.mark.django_db


@pytest.mark.django_db
class TestUserProfileModel:
    def test_user_profile_get_absolute_url(self, profile):
        assert (
            profile.get_absolute_url()
            == f"/profile/user/{profile.user.username}"
        )


class TestIncrementDerpsRated:
    def test_count_increments(self, profile):
        assert profile.n_derps_rated == 0
        profile.increment_n_derps_rated()
        p = DerpUserProfile.objects.get(user=profile.user)
        assert p.n_derps_rated == 1
