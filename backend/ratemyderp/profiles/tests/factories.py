import pytest
import factory, factory.fuzzy
from factory.django import DjangoModelFactory, mute_signals
from django.db.models.signals import post_save, pre_save

from ..models import DerpUserProfile
from ratemyderp.users.tests.factories import DerpUserFactory

pytestmark = pytest.mark.django_db


@mute_signals(pre_save, post_save)
class UserProfileFactory(DjangoModelFactory):
    class Meta:
        model = DerpUserProfile

    user = factory.SubFactory(DerpUserFactory)
    bio = factory.Faker("paragraph", variable_nb_sentences=True)
    avatar = factory.django.ImageField(color="blue", width=1080, height=1080)
    is_private = factory.fuzzy.FuzzyChoice([True, False])


@pytest.fixture
def profile():
    return UserProfileFactory()
