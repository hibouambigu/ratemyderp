from django.contrib import admin

from ratemyderp.profiles.models import DerpUserProfile


@admin.register(DerpUserProfile)
class DerpUserProfileAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "is_private",
        "n_derps_rated",
        "n_derps_submitted",
        "is_flagged",
        "n_flags",
    )
    list_filter = ("is_private", "is_flagged")
    search_fields = ("user",)
    list_per_page = 25
