from django.db import models
from model_utils.models import TimeStampedModel
from django.contrib.auth import get_user_model
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

User = get_user_model()


class DerpUserProfile(TimeStampedModel):
    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"

    # user model
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="profile"
    )
    # avatar/image
    avatar = ProcessedImageField(
        upload_to="avatars/%Y/%m/%d/",
        processors=[ResizeToFill(160, 160)],
        format="JPEG",
        options={"quality": 70},
    )

    # private profile flag
    is_private = models.BooleanField(default=False)
    # biography
    bio = models.TextField(max_length=200, blank=True)
    # stats
    n_derps_rated = models.IntegerField(default=0, editable=False, blank=True)
    n_derps_submitted = models.IntegerField(
        default=0, editable=False, blank=True
    )
    # inappropriate content flags
    is_flagged = models.BooleanField(default=False, blank=True)
    n_flags = models.IntegerField(default=0, editable=False, blank=True)

    next_derp_id = models.CharField(
        default="", editable=False, blank=True, max_length=60
    )
    "cached next derp to rate"
    # ^ modelled as charfield b/c postgres doesn't allow empty string
    #   to be stored in UUIDField

    def __str__(self):
        return f"{self.user.username}'s profile"

    def get_absolute_url(self):
        return self.user.get_absolute_url()

    def increment_n_derps_rated(self):
        "Increments the count for number of derps rated."
        self.n_derps_rated += 1
        self.save()
