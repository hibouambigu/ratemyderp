from django.urls import path
from ratemyderp.profiles.views import (
    user_profile_view,
    user_profile_update_view,
)

app_name = "profile"
urlpatterns = [
    path(
        route="settings",
        view=user_profile_update_view,
        name="settings",
    ),
    path(route="user/<str:username>", view=user_profile_view, name="detail"),
]
